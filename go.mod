module gitlab.com/kantai/keyraser/go-client

go 1.20

require (
	github.com/bits-and-blooms/bloom/v3 v3.3.1
	github.com/json-iterator/go v1.1.12
	github.com/orcaman/concurrent-map/v2 v2.0.1
	github.com/rs/zerolog v1.29.1
	github.com/stretchr/testify v1.8.2
	gitlab.com/kantai/keyraser/auth v0.5.0
	gitlab.com/kantai/keyraser/protocol v0.5.0
	golang.org/x/crypto v0.13.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/bits-and-blooms/bitset v1.3.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
