package utils

import (
	"flag"
	"os"
)

var inputFilePath string
var outputFilePath string

func DefineIoFlags(set *flag.FlagSet) {
	set.StringVar(&inputFilePath, "input", "-", "The file to read the input from, - to read from stdin")
	set.StringVar(&outputFilePath, "output", "-", "The file to write the output to, - to write to stdout")
}

func InputFile() (*os.File, error) {
	if inputFilePath == "-" {
		return os.Stdin, nil
	} else {
		return os.Open(inputFilePath)
	}
}

func OutputFile() (*os.File, error) {
	if outputFilePath == "-" {
		return os.Stdout, nil
	} else {
		return os.Create(outputFilePath)
	}
}
