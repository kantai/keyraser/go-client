package utils

import (
	"flag"
	"github.com/rs/zerolog"
)

var level uint = uint(zerolog.WarnLevel)

func DefineLoggingFlags(set *flag.FlagSet) {
	set.UintVar(&level, "loglevel", uint(zerolog.WarnLevel), "The log level, 0=Debug,1=Info,2=Warn,3=Error")
}

func SetupLogging() {
	zerolog.SetGlobalLevel(zerolog.Level(level))
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs
}
