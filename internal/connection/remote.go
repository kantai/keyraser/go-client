package connection

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"flag"
	cmap "github.com/orcaman/concurrent-map/v2"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/auth"
	"gitlab.com/kantai/keyraser/go-client/pkg/interfaces"
	"gitlab.com/kantai/keyraser/protocol"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"google.golang.org/protobuf/proto"
	"sync/atomic"
)

const numberOfParallelResponseHandlers = 5

type RemoteKeyStoreConfig struct {
	keystoreAddress    string
	credentialFilePath string
}

func (c *RemoteKeyStoreConfig) DefineFlags(flagSet *flag.FlagSet) {
	flagSet.StringVar(&c.keystoreAddress, "address", "127.0.0.1:1996", "The address of the keystore to which the client connects")
	auth.AddCredentialFlag(flagSet, &c.credentialFilePath)
}

func (c *RemoteKeyStoreConfig) Connect() (interfaces.Connection, error) {
	cred, err := auth.NewCredential(c.credentialFilePath)
	if err != nil {
		log.Error().Err(err).Msgf("error loading credentials")
		return nil, err
	}
	clientCred, ok := cred.(auth.ClientCredential)
	if !ok {
		log.Error().Err(err).Msgf("no connection credential")
		return nil, err
	}
	tlsConfig, err := cred.TlsConfig(auth.KeyStore)
	if err != nil {
		log.Error().Err(err).Msgf("error creating tls configuration")
		return nil, err
	}

	tlsConfig.InsecureSkipVerify = false
	conn, err := tls.Dial("tcp", c.keystoreAddress, tlsConfig)
	if err != nil {
		log.Error().Err(err).Msgf("error connecting to %s", c.keystoreAddress)
		return nil, err
	}
	logger := log.With().Str("remote", conn.RemoteAddr().String()).Str("local", conn.LocalAddr().String()).Logger()
	logger.Debug().Msgf("keystore: connected to: %s", conn.RemoteAddr())
	if logger.GetLevel() <= zerolog.DebugLevel {
		state := conn.ConnectionState()
		var publicKeys []string
		for _, v := range state.PeerCertificates {
			pubKey, err := x509.MarshalPKIXPublicKey(v.PublicKey)
			if err != nil {
				logger.Warn().Err(err).Msg("invalid public key")
			} else {
				publicKeys = append(publicKeys, string(pubKey))
			}
		}
		logger.Debug().Strs("publicKeys", publicKeys).Msg("connection: server public keys")
	}
	frameWriter := protocol.NewFrameWriter(conn)
	frameReader := protocol.NewFrameReader(conn)

	handlerMap := cmap.NewWithCustomShardingFunction[int64, handlers](shardFromRequestId)

	connection := &remoteConnection{
		credential:       clientCred,
		tlsConnection:    conn,
		writer:           frameWriter,
		reader:           frameReader,
		receiveBuffer:    make([]byte, 8192),
		responseHandlers: handlerMap,
		waitingResponses: make(chan *generated.Response, numberOfParallelResponseHandlers),
		logger:           logger,
		closed:           atomic.Bool{},
	}
	go connection.parseResponses()
	for i := 0; i < numberOfParallelResponseHandlers; i++ {
		go connection.handleResponse(connection.waitingResponses)
	}
	return connection, nil
}

func shardFromRequestId(requestId int64) uint32 {
	return uint32(requestId)
}

type handlers struct {
	responseHandler interface{}
	errorHandler    interfaces.ErrorHandler
}

type remoteConnection struct {
	credential       auth.ClientCredential
	tlsConnection    *tls.Conn
	writer           protocol.FrameWriter
	reader           protocol.FrameReader
	responseHandlers cmap.ConcurrentMap[int64, handlers]
	lastRequestId    int64
	receiveBuffer    []byte
	waitingResponses chan *generated.Response
	logger           zerolog.Logger
	closed           atomic.Bool
}

func (c *remoteConnection) sendRequest(request *generated.Request) error {
	requestBuffer, err := proto.Marshal(request)
	if err != nil {
		c.logger.Error().Err(err).Msg("error encoding request")
		return err
	}

	err = c.writer.WriteFrame(requestBuffer)
	if err != nil {
		c.logger.Error().Err(err).Msg("error writing request")
		return err
	}

	return nil
}

func (c *remoteConnection) parseResponses() {
	for {
		if c.reader.Next() {
			err := c.reader.Error()
			if err != nil {
				c.logger.Error().Err(err).Msg("error reading next frame")
				continue
			}
			frame := c.reader.GetFrame()
			frameContent := frame.Content()
			response := &generated.Response{}
			err = proto.Unmarshal(frameContent, response)
			if err != nil {
				c.logger.Error().Err(err).Msg("error decoding response")
			}
			c.waitingResponses <- response
		} else {
			if !c.closed.Load() && c.reader.Error() != nil {
				c.logger.Error().Err(c.reader.Error()).Stack().Msg("error reading frame response")
			}
			break
		}
	}
	c.logger.Info().Msg("connection closed, no more responses to parse")
	close(c.waitingResponses)
}

func (c *remoteConnection) handleResponse(responses chan *generated.Response) {
Loop:
	for {
		select {
		case res, ok := <-responses:

			if !ok {
				c.logger.Info().Msg("no more responses, close handler")
				break Loop
			}

			requestId := res.RequestId
			c.logger.Debug().Msgf("handling response for request %d", requestId)

			handlers, exists := c.responseHandlers.Pop(requestId)
			if !exists {
				c.logger.Warn().Msgf("unexpected response to request %d", requestId)
				continue
			}
			switch typedResponse := res.KeyResponse.(type) {
			case *generated.Response_EncKeyResponse:
				handler := handlers.responseHandler.(interfaces.ResponseHandler[*generated.EncryptionKeyResponse])
				handler(typedResponse.EncKeyResponse)
			case *generated.Response_DecKeyResponse:
				handler := handlers.responseHandler.(interfaces.ResponseHandler[*generated.DecryptionKeyResponse])
				handler(typedResponse.DecKeyResponse)
			case *generated.Response_KeyPermissionResponse:
				handler := handlers.responseHandler.(interfaces.ResponseHandler[*generated.KeyPermissionResponse])
				handler(typedResponse.KeyPermissionResponse)
			case *generated.Response_CreateKeyResponse:
				handler := handlers.responseHandler.(interfaces.ResponseHandler[*generated.CreateKeyResponse])
				handler(typedResponse.CreateKeyResponse)
			case *generated.Response_DeleteKeyResponse:
				handler := handlers.responseHandler.(interfaces.ResponseHandler[*generated.DeleteKeyResponse])
				handler(typedResponse.DeleteKeyResponse)
			case *generated.Response_ErrorResponse:
				errorResponse := res.GetErrorResponse()
				handlers.errorHandler(nil, errors.New(errorResponse.Errors[0].Code.String()))
			}
			//fmt.Println(res)
			//case <-time.After(1 * time.Second):
			//	fmt.Println("timeout 1")
		}

	}
}

func (c *remoteConnection) getNextRequestId() int64 {
	for {
		val := atomic.LoadInt64(&c.lastRequestId)
		if atomic.CompareAndSwapInt64(&c.lastRequestId, val, val+1) {
			return val
		}
	}
}

func (c *remoteConnection) Credential() auth.ClientCredential {
	return c.credential
}

func (c *remoteConnection) RequestEncryptionKeys(encKeyRequest *generated.EncryptionKeyRequest, handler interfaces.ResponseHandler[*generated.EncryptionKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	request := &generated.Request{
		KeyRequest: &generated.Request_EncKeyRequest{
			EncKeyRequest: encKeyRequest,
		},
	}
	requestId := c.getNextRequestId()
	request.RequestId = requestId
	c.responseHandlers.Set(requestId, handlers{
		responseHandler: handler,
		errorHandler:    errorHandler,
	})

	err := c.sendRequest(request)
	if err != nil {
		errorHandler(nil, err)
		c.responseHandlers.Pop(request.RequestId)
	}
	return requestId, nil
}

func (c *remoteConnection) RequestDecryptionKeys(decKeyRequest *generated.DecryptionKeyRequest, handler interfaces.ResponseHandler[*generated.DecryptionKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	request := &generated.Request{
		KeyRequest: &generated.Request_DecKeyRequest{
			DecKeyRequest: decKeyRequest,
		},
	}
	requestId := c.getNextRequestId()
	request.RequestId = requestId
	c.responseHandlers.Set(requestId, handlers{
		responseHandler: handler,
		errorHandler:    errorHandler,
	})

	err := c.sendRequest(request)
	if err != nil {
		errorHandler(nil, err)
		c.responseHandlers.Pop(request.RequestId)
	}
	return requestId, nil
}

func (c *remoteConnection) RequestCreateKeys(createKeyRequest *generated.CreateKeyRequest, handler interfaces.ResponseHandler[*generated.CreateKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	request := &generated.Request{
		KeyRequest: &generated.Request_CreateKeyRequest{
			CreateKeyRequest: createKeyRequest,
		},
	}
	requestId := c.getNextRequestId()
	request.RequestId = requestId
	c.responseHandlers.Set(requestId, handlers{
		responseHandler: handler,
		errorHandler:    errorHandler,
	})

	err := c.sendRequest(request)
	if err != nil {
		errorHandler(nil, err)
		c.responseHandlers.Pop(request.RequestId)
	}
	return requestId, nil
}

func (c *remoteConnection) RequestDeleteKeys(deleteKeyRequest *generated.DeleteKeyRequest, handler interfaces.ResponseHandler[*generated.DeleteKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	request := &generated.Request{
		KeyRequest: &generated.Request_DeleteKeyRequest{
			DeleteKeyRequest: deleteKeyRequest,
		},
	}
	requestId := c.getNextRequestId()
	request.RequestId = requestId
	c.responseHandlers.Set(requestId, handlers{
		responseHandler: handler,
		errorHandler:    errorHandler,
	})

	err := c.sendRequest(request)
	if err != nil {
		errorHandler(nil, err)
		c.responseHandlers.Pop(request.RequestId)
	}
	return requestId, nil
}

func (c *remoteConnection) RequestKeyPermissions(keyPermissionRequest *generated.KeyPermissionRequest, handler interfaces.ResponseHandler[*generated.KeyPermissionResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	request := &generated.Request{
		KeyRequest: &generated.Request_KeyPermissionRequest{
			KeyPermissionRequest: keyPermissionRequest,
		},
	}
	requestId := c.getNextRequestId()
	request.RequestId = requestId
	c.responseHandlers.Set(requestId, handlers{
		responseHandler: handler,
		errorHandler:    errorHandler,
	})

	err := c.sendRequest(request)
	if err != nil {
		errorHandler(nil, err)
		c.responseHandlers.Pop(request.RequestId)
	}
	return requestId, nil
}

func (c *remoteConnection) RequestUpdateKeyPermissions(updateKeyPermissionRequest *generated.UpdateKeyPermissionRequest, handler interfaces.ResponseHandler[*generated.KeyPermissionResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	request := &generated.Request{
		KeyRequest: &generated.Request_UpdateKeyPermissionRequest{
			UpdateKeyPermissionRequest: updateKeyPermissionRequest,
		},
	}
	requestId := c.getNextRequestId()
	request.RequestId = requestId
	c.responseHandlers.Set(requestId, handlers{
		responseHandler: handler,
		errorHandler:    errorHandler,
	})

	err := c.sendRequest(request)
	if err != nil {
		errorHandler(nil, err)
		c.responseHandlers.Pop(request.RequestId)
	}
	return requestId, nil
}

func (c *remoteConnection) Close() {
	c.closed.Store(true)
	c.logger.Info().Msgf("closing connection %s", c.tlsConnection.LocalAddr().String())
	c.tlsConnection.Close()
}
