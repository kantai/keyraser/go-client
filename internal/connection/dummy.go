package connection

import (
	"gitlab.com/kantai/keyraser/auth"
	"gitlab.com/kantai/keyraser/go-client/pkg/interfaces"
	"gitlab.com/kantai/keyraser/protocol/generated"
)

type DummyConnection struct {
	Cred   auth.ClientCredential
	DecKey *generated.DecryptionKey
	EncKey generated.EncryptionKey
}

func (c *DummyConnection) Credential() auth.ClientCredential {
	return c.Cred
}

func (c *DummyConnection) RequestEncryptionKeys(request *generated.EncryptionKeyRequest, handler interfaces.ResponseHandler[*generated.EncryptionKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {

	response := generated.EncryptionKeyResponse{}
	for _, entityId := range request.EntityIds {
		encKey := &c.EncKey
		encKey.EntityId = entityId
		response.Keys = append(response.Keys, encKey)
	}
	go func() {
		handler(&response)
	}()
	return 1, nil
}

func (c *DummyConnection) RequestDecryptionKeys(request *generated.DecryptionKeyRequest, handler interfaces.ResponseHandler[*generated.DecryptionKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	response := generated.DecryptionKeyResponse{}
	for _, keyId := range request.KeyIds {
		if c.DecKey != nil {
			decKey := c.DecKey
			decKey.KeyId = keyId
			response.Keys = append(response.Keys, decKey)
		}
	}
	go func() {
		handler(&response)
	}()
	return 1, nil
}

func (c *DummyConnection) RequestCreateKeys(request *generated.CreateKeyRequest, handler interfaces.ResponseHandler[*generated.CreateKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	return 1, nil
}

func (c *DummyConnection) RequestDeleteKeys(request *generated.DeleteKeyRequest, handler interfaces.ResponseHandler[*generated.DeleteKeyResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	return 1, nil
}

func (c *DummyConnection) RequestKeyPermissions(request *generated.KeyPermissionRequest, handler interfaces.ResponseHandler[*generated.KeyPermissionResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	return 1, nil
}

func (c *DummyConnection) RequestUpdateKeyPermissions(request *generated.UpdateKeyPermissionRequest, handler interfaces.ResponseHandler[*generated.KeyPermissionResponse], errorHandler interfaces.ErrorHandler) (int64, error) {
	return 1, nil
}

func (c *DummyConnection) Close() {

}
