package block

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncryptedBlockBuilderCreateEmptyBlock(t *testing.T) {
	assert := assert.New(t)
	builder, err := newEncryptedBlockBuilder(0)
	assert.Nil(err)

	block := builder.ToBlock()

	assert.NotNil(block)
	assert.Equal(uint8(1), block.Version())
	assert.Equal(uint8(48), block.HeaderLength())
	assert.Equal(uint16(16), block.ContentLength())
	assert.Equal(uint(64), block.Size())
}

func TestEncryptedBlockBuilderSetNonceSuccessful(t *testing.T) {
	assert := assert.New(t)
	builder, err := newEncryptedBlockBuilder(0)
	assert.Nil(err)

	nonce := []byte{
		0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,
	}
	err = builder.SetNonce(nonce)
	assert.Nil(err)
	block := builder.ToBlock()

	assert.NotNil(block)
	assert.Equal(nonce, builder.NonceBuffer())
	assert.Equal(nonce, block.Nonce())
}

func TestEncryptedBlockBuilderSetNonceToShort(t *testing.T) {
	assert := assert.New(t)
	builder, err := newEncryptedBlockBuilder(0)
	assert.Nil(err)

	nonce := []byte{0x02}

	err = builder.SetNonce(nonce)
	assert.NotNil(err)
	assert.EqualError(err, "invalid nonce size, expected 12, found 1")
}

func TestEncryptedBlockBuilderSetKeyIdSuccessful(t *testing.T) {
	assert := assert.New(t)
	builder, err := newEncryptedBlockBuilder(0)
	assert.Nil(err)

	keyId := []byte{
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	}
	err = builder.SetKeyId(keyId)
	assert.Nil(err)
	block := builder.ToBlock()

	assert.NotNil(block)
	assert.Equal(keyId, block.KeyId())
	assert.Equal(keyId, builder.KeyIdBuffer())
}

func TestEncryptedBlockBuilderSetInvalidKeyId(t *testing.T) {
	assert := assert.New(t)
	builder, err := newEncryptedBlockBuilder(0)
	assert.Nil(err)

	keyId := []byte{0x01}

	err = builder.SetKeyId(keyId)
	assert.NotNil(err)
	assert.EqualError(err, "invalid keyId size, expected 32, found 1")
}

func TestEncryptedBlockBuilderDataBufferForEmptyData(t *testing.T) {
	assert := assert.New(t)
	builder, err := newEncryptedBlockBuilder(0)
	assert.Nil(err)

	buffer := builder.DataBuffer()
	assert.NotNil(buffer)
	assert.Len(buffer, TagSize)
}

func TestEncryptedBlockBuilderDataBufferForNonEmptyData(t *testing.T) {
	assert := assert.New(t)
	dataSize := 1024
	builder, err := newEncryptedBlockBuilder(uint16(dataSize))
	assert.Nil(err)

	buffer := builder.DataBuffer()
	assert.NotNil(buffer)
	assert.Len(buffer, dataSize+TagSize)
}

func TestEncryptedBlockBuilderDataBufferForMaxLengthData(t *testing.T) {
	assert := assert.New(t)
	dataSize := MaxEncryptedDataLength
	builder, err := newEncryptedBlockBuilder(uint16(dataSize))
	assert.Nil(err)

	buffer := builder.DataBuffer()
	assert.NotNil(buffer)
	assert.Len(buffer, dataSize+TagSize)
}

func TestEncryptedBlockBuilderDataBufferForTooLargeLengthData(t *testing.T) {
	assert := assert.New(t)
	dataSize := MaxEncryptedDataLength + 1
	builder, err := newEncryptedBlockBuilder(uint16(dataSize))
	assert.Errorf(err, "too large data length, len (65520) > max (65519)")
	assert.Nil(builder)
}

func TestParseBlockFromTooSmall(t *testing.T) {
	assert := assert.New(t)
	buffer := []byte{}

	block, err := ParseDataBlock(buffer)

	assert.Nil(block)
	assert.NotNil(err)
	assert.EqualError(err, "buffer too small to check block size")
}

func TestParseBlockUnknownVersion(t *testing.T) {
	assert := assert.New(t)
	buffer := []byte{
		// Version
		0x16,

		// Header Length
		0x28,

		// Data Length,
		0x00, 0x04,
	}

	block, err := ParseDataBlock(buffer)

	assert.Nil(block)
	assert.NotNil(err)
	assert.EqualError(err, "unknown block version 22")
}

func TestParseBlockUnexpectedHeaderLength(t *testing.T) {
	assert := assert.New(t)
	buffer := []byte{
		// Version
		0x01,

		// Header Length
		0x42,

		// Data Length,
		0x00, 0x04,
	}

	block, err := ParseDataBlock(buffer)

	assert.Nil(block)
	assert.NotNil(err)
	assert.EqualError(err, "unexpected header size, expected 48, found 66")
}

func TestParseBlockIncompleteBlock(t *testing.T) {
	assert := assert.New(t)
	buffer := []byte{
		// Version
		0x01,

		// Header Length
		0x30,

		// Data Length,
		0x00, 0x04,
	}

	block, err := ParseDataBlock(buffer)

	assert.Nil(block)
	assert.NotNil(err)
	assert.EqualError(err, "incomplete block, required buffer size 52")
}

func TestParseBlockFromBuffer(t *testing.T) {
	assert := assert.New(t)
	buffer := []byte{
		// Version
		0x01,

		// Header Length
		0x30,

		// Data Length,
		0x00, 0x14,

		// Nonce
		0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,

		// KeyId
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,

		// Encrypted data
		0x05, 0x05, 0x05, 0x05,

		// Tag,
		0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
		0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
	}

	block, err := ParseDataBlock(buffer)

	assert.NotNil(block)
	assert.Nil(err)

	version := block.Version()
	assert.Equal(uint8(1), version)

	headerLength := block.HeaderLength()
	assert.Equal(uint8(0x30), headerLength)

	encBlock, ok := block.(EncryptedBlock)
	assert.True(ok)

	keyId := encBlock.KeyId()
	assert.Equal([]byte{0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}, keyId)

	nonce := encBlock.Nonce()
	assert.Equal([]byte{0x02, 0x02, 0x02, 0x02,
		0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02}, nonce)

	dataLength := encBlock.DataLength()
	assert.Equal(uint16(4), dataLength)

	data := encBlock.EncryptedData()
	assert.Equal([]byte{0x05, 0x05, 0x05, 0x05}, data)

	tag := encBlock.Tag()
	assert.Equal([]byte{0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
		0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06}, tag)

	blockSize := block.Size()
	assert.Equal(uint(0x44), blockSize)
}

func TestParseBlockWithMaxSize(t *testing.T) {
	assert := assert.New(t)
	var headerLength uint8 = 0x30
	maxContentLength := 0xffff
	tagLength := 16
	maxLength := int(headerLength) + maxContentLength
	buffer := make([]byte, maxLength)
	copy(buffer, []byte{
		// Version
		0x01,

		// Header Length
		headerLength,

		// Data Length,
		0xff, 0xff,

		// Nonce
		0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,

		// KeyId
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	})

	copy(buffer[maxLength-tagLength:], []byte{
		// Tag,
		0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
		0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
	})

	block, err := ParseDataBlock(buffer)

	assert.NotNil(block)
	assert.Nil(err)

	version := block.Version()
	assert.Equal(uint8(1), version)

	parsedHeaderLength := block.HeaderLength()
	assert.Equal(uint8(0x30), parsedHeaderLength)

	contentLength := block.ContentLength()
	assert.Equal(uint16(maxContentLength), contentLength)

	encBlock, ok := block.(EncryptedBlock)
	assert.True(ok)

	keyId := encBlock.KeyId()
	assert.Equal([]byte{0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01}, keyId)

	nonce := encBlock.Nonce()
	assert.Equal([]byte{0x02, 0x02, 0x02, 0x02,
		0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02}, nonce)

	dataLength := encBlock.DataLength()
	assert.Equal(uint16(maxContentLength-tagLength), dataLength)

	data := encBlock.EncryptedData()
	assert.Equal(dataLength, uint16(len(data)))

	dataAndTag := encBlock.EncryptedDataAndTag()
	assert.Equal(maxContentLength, len(dataAndTag))

	tag := encBlock.Tag()
	assert.Equal([]byte{0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
		0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06}, tag)

	blockSize := block.Size()
	assert.Equal(uint(maxLength), blockSize)
}
