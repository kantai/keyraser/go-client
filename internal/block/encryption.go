package block

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"

	"gitlab.com/kantai/keyraser/protocol/generated"
	chacha "golang.org/x/crypto/chacha20poly1305"
)

func EncryptData(data []byte, encKey *generated.EncryptionKey, nonceSources ...io.Reader) (EncryptedBlock, error) {
	dataSize := len(data)

	builder, err := newEncryptedBlockBuilder(uint16(dataSize))
	if err != nil {
		return nil, err
	}
	nonceBuffer := builder.NonceBuffer()
	keyIdBuffer := builder.KeyIdBuffer()
	dataBuffer := builder.DataBuffer()

	var nonceSource io.Reader
	if len(nonceSources) > 0 {
		nonceSource = nonceSources[0]
	} else {
		nonceSource = GetNonceReader()
	}
	key, err := chacha.New(encKey.KeyBytes)
	if err != nil {
		return nil, fmt.Errorf("invalid key bytes %w", err)
	}

	keyId := encKey.KeyId

	copy(keyIdBuffer, keyId)
	if _, err := io.ReadFull(nonceSource, nonceBuffer); err != nil {
		return nil, fmt.Errorf("error getting random nonce: %w", err)
	}

	key.Seal(dataBuffer[:0], nonceBuffer, data, nil)

	return builder.ToBlock(), nil
}

func DecryptData(block EncryptedBlock, decKey *generated.DecryptionKey, destBuffer []byte) error {
	keyId := decKey.KeyId
	if !bytes.Equal(keyId, block.KeyId()) {
		return fmt.Errorf("invalid key for decryption given, expected %s, given %s", hex.EncodeToString(block.KeyId()), hex.EncodeToString(keyId))
	}
	if int(block.DataLength()) > len(destBuffer) {
		return fmt.Errorf("too small destination buffer, needed %d bytes, given %d", block.DataLength(), len(destBuffer))
	}

	key, err := chacha.New(decKey.KeyBytes)
	if err != nil {
		return err
	}

	_, err = key.Open(destBuffer[:0], block.Nonce(), block.EncryptedDataAndTag(), nil)
	return err
}
