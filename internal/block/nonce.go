package block

import (
	"crypto/rand"
	"io"
)

var nonceReader io.Reader

func init() {
	nonceReader = rand.Reader
}

func SetNonceReader(newReader io.Reader) {
	nonceReader = newReader
}

func GetNonceReader() io.Reader {
	return nonceReader
}
