package block

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"testing"
)

var testKey = []byte{
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
	17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
}
var keyId = []byte{
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
}

var wrongKeyId = []byte{
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
}

var blockBuffer = []byte{
	// Version
	0x01,

	// Header Length
	0x30,

	// Data Length,
	0x00, 0x18,

	// Nonce
	0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c,

	// KeyId
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,

	// Encrypted data
	0xc9, 0x76, 0x4e, 0x68, 0x5d, 0xda, 0x57, 0x97,

	// Tag,
	0x7d, 0x74, 0x17, 0x26, 0x84, 0xae, 0xfc, 0x74,
	0x2f, 0x30, 0xe7, 0xc2, 0x30, 0xde, 0x6f, 0xb2,
}

const wrongVariant = 0x01

var testNonce = []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}

func TestEncryptData(t *testing.T) {
	assert := assert.New(t)
	data := []byte{1, 2, 3, 4, 5, 6, 7, 8}

	encryptionKey := &generated.EncryptionKey{
		KeyId:    keyId,
		KeyBytes: testKey,
	}
	block, err := EncryptData(data, encryptionKey, bytes.NewReader(testNonce))
	assert.Nil(err)

	assert.Len(block.BlockData(), 72)
	assert.Equal(uint(72), block.Size())

	assert.Equal(keyId, []uint8(block.KeyId())[0:32])
	assert.Equal(testNonce, block.Nonce())
	assert.Equal(uint16(8), block.DataLength())
	assert.Equal([]byte{0xc9, 0x76, 0x4e, 0x68, 0x5d, 0xda, 0x57, 0x97}, block.EncryptedData())
	assert.Equal([]byte{0x7d, 0x74, 0x17, 0x26, 0x84, 0xae, 0xfc, 0x74, 0x2f, 0x30, 0xe7, 0xc2, 0x30, 0xde, 0x6f, 0xb2}, block.Tag())
}

func TestEncryptDataWithInvalidKey(t *testing.T) {
	assert := assert.New(t)
	data := []byte{1, 2, 3, 4, 5, 6, 7, 8}

	encryptionKey := &generated.EncryptionKey{
		KeyId:    keyId,
		KeyBytes: []byte{0, 1, 2},
	}
	encryptedData, err := EncryptData(data, encryptionKey)
	assert.EqualError(err, "invalid key bytes chacha20poly1305: bad key length")

	assert.Nil(encryptedData)
}

func TestEncryptDataWithTooLargeData(t *testing.T) {
	assert := assert.New(t)
	data := make([]byte, 0xFFFF)

	encryptedData, err := EncryptData(data, nil)
	assert.EqualError(err, "too large data length, len (65535) > max (65519)")

	assert.Nil(encryptedData)
}

func TestDecryptDataWithWrongKey(t *testing.T) {
	assert := assert.New(t)
	destBuffer := []byte{0, 0, 0, 0, 0, 0, 0, 0}

	decryptionKey := &generated.DecryptionKey{
		KeyId:    wrongKeyId,
		KeyBytes: testKey,
	}

	block, err := ParseDataBlock(blockBuffer)

	assert.Nil(err)

	err = DecryptData(block.(EncryptedBlock), decryptionKey, destBuffer)
	assert.EqualError(err, "invalid key for decryption given, expected 0101010101010101010101010101010101010101010101010101010101010101, given 0101010101010101010101010101010101010101010101010000000000000001")
}

func TestDecryptDataWithInvalidKey(t *testing.T) {
	assert := assert.New(t)
	destBuffer := []byte{0, 0, 0, 0, 0, 0, 0, 0}

	decryptionKey := &generated.DecryptionKey{
		KeyId:    keyId,
		KeyBytes: []byte{0x00},
	}

	block, _ := ParseDataBlock(blockBuffer)

	err := DecryptData(block.(EncryptedBlock), decryptionKey, destBuffer)
	assert.EqualError(err, "chacha20poly1305: bad key length")
}

func TestDecryptDataWithTooSmallBuffer(t *testing.T) {
	assert := assert.New(t)
	destBuffer := []byte{0}

	decryptionKey := &generated.DecryptionKey{
		KeyId:    keyId,
		KeyBytes: testKey,
	}

	block, _ := ParseDataBlock(blockBuffer)

	err := DecryptData(block.(EncryptedBlock), decryptionKey, destBuffer)
	assert.EqualError(err, "too small destination buffer, needed 8 bytes, given 1")
}

func TestDecryptDataWithWrongTag(t *testing.T) {
	assert := assert.New(t)
	destBuffer := []byte{0, 0, 0, 0, 0, 0, 0, 0}

	decryptionKey := &generated.DecryptionKey{
		KeyId:    keyId,
		KeyBytes: testKey,
	}

	tmpBuffer := make([]byte, len(blockBuffer))
	copy(tmpBuffer, blockBuffer)
	block, _ := ParseDataBlock(tmpBuffer)
	encBlock, _ := block.(EncryptedBlock)
	encBlock.Tag()[0] = 0x01

	err := DecryptData(encBlock, decryptionKey, destBuffer)
	assert.NotNil(err)
	assert.EqualError(err, "chacha20poly1305: message authentication failed")
}

func TestDecryptValidBlock(t *testing.T) {
	assert := assert.New(t)
	destBuffer := []byte{0, 0, 0, 0, 0, 0, 0, 0}

	decryptionKey := &generated.DecryptionKey{
		KeyId:    keyId,
		KeyBytes: testKey,
	}

	block, _ := ParseDataBlock(blockBuffer)

	err := DecryptData(block.(EncryptedBlock), decryptionKey, destBuffer)
	assert.Nil(err)
	assert.Equal([]byte{1, 2, 3, 4, 5, 6, 7, 8}, destBuffer)
}
