package block

import (
	"encoding/binary"
	"fmt"
)

// Common block header
const VersionSize = 1
const versionPosition = 0

const HeaderLengthSize = 1
const headerLengthPosition = versionPosition + 1

const ContentLengthSize = 2
const contentLengthStart = headerLengthPosition + 1
const contentLengthEnd = contentLengthStart + ContentLengthSize

const MaxContentLength = 0xffff
const requiredHeaderSizeCheck = contentLengthEnd

// Encrypted block header
const NonceSize = 12
const nonceStart = contentLengthEnd
const nonceEnd = nonceStart + NonceSize

const KeyIdSize = 32
const keyIdStart = nonceEnd
const keyIdEnd = keyIdStart + KeyIdSize

const TagSize = 16

const HeaderSizeEncrypted = VersionSize + HeaderLengthSize + ContentLengthSize + NonceSize + KeyIdSize
const dataStartEncrypted = HeaderSizeEncrypted

const VersionEncrypted = 1

const MaxEncryptedDataLength = MaxContentLength - TagSize

type IncompleteBlock struct {
	BufferSize uint
}

type Block interface {
	Version() uint8
	HeaderLength() uint8
	ContentLength() uint16
	Size() uint
	BlockData() []byte
}

type EncryptedBlock interface {
	Block
	KeyId() []byte
	Nonce() []byte
	DataLength() uint16
	EncryptedData() []byte
	EncryptedDataAndTag() []byte
	Tag() []byte
}

func (e *IncompleteBlock) Error() string {
	return fmt.Sprintf("incomplete block, required buffer size %d", e.BufferSize)
}

type blockBuilder struct {
	buffer []byte
}

func (b *blockBuilder) setVersion(version uint8) {
	b.buffer[versionPosition] = version
}

func (b *blockBuilder) setHeaderLength(headerLength uint8) {
	b.buffer[headerLengthPosition] = headerLength
}

func (b *blockBuilder) setContentLength(contentLength uint16) {
	binary.BigEndian.PutUint16(b.buffer[contentLengthStart:contentLengthEnd], contentLength)
}

func (b *blockBuilder) SetNonce(nonce []byte) error {
	if len(nonce) != NonceSize {
		return fmt.Errorf("invalid nonce size, expected %d, found %d", NonceSize, len(nonce))
	}
	copy(b.buffer[nonceStart:nonceEnd], nonce)
	return nil
}

func (b *blockBuilder) NonceBuffer() []byte {
	return b.buffer[nonceStart:nonceEnd]
}

func (b *blockBuilder) SetKeyId(keyId []byte) error {
	if len(keyId) != KeyIdSize {
		return fmt.Errorf("invalid keyId size, expected %d, found %d", KeyIdSize, len(keyId))
	}
	copy(b.buffer[keyIdStart:keyIdEnd], keyId)
	return nil
}

func (b *blockBuilder) KeyIdBuffer() []byte {
	return b.buffer[keyIdStart:keyIdEnd]
}

func (b *blockBuilder) DataBuffer() []byte {
	return b.buffer[b.buffer[headerLengthPosition]:]
}

func (b *blockBuilder) ToBlock() EncryptedBlock {
	encBlock := encryptedDataBlock(b.buffer)
	return &encBlock
}

func newEncryptedBlockBuilder(dataLength uint16) (*blockBuilder, error) {
	if dataLength > MaxEncryptedDataLength {
		return nil, fmt.Errorf("too large data length, len (%d) > max (%d)", dataLength, MaxEncryptedDataLength)
	}

	blockSize := HeaderSizeEncrypted + int(dataLength) + TagSize
	builder := &blockBuilder{
		buffer: make([]byte, blockSize),
	}
	builder.setVersion(VersionEncrypted)
	builder.setHeaderLength(HeaderSizeEncrypted)
	builder.setContentLength(dataLength + TagSize)

	return builder, nil
}

func GetSizeOfDataBlock(buffer []byte) (uint, error) {
	if len(buffer) < requiredHeaderSizeCheck {
		return 0, fmt.Errorf("buffer too small to check block size")
	}

	block := commonDataBlock(buffer)
	expectedHeaderSize := uint8(0)
	switch block.Version() {
	case VersionEncrypted:
		expectedHeaderSize = HeaderSizeEncrypted

	default:
		return 0, fmt.Errorf("unknown block version %d", block.Version())
	}

	if block.HeaderLength() != expectedHeaderSize {
		return 0, fmt.Errorf("unexpected header size, expected %d, found %d", expectedHeaderSize, block.HeaderLength())
	}

	contentLength := block.ContentLength()
	return uint(expectedHeaderSize) + uint(contentLength), nil

}

func ParseDataBlock(buffer []byte) (Block, error) {
	completeBlockSize, err := GetSizeOfDataBlock(buffer)
	if err != nil {
		return nil, err
	}
	if uint(len(buffer)) < completeBlockSize {
		return nil, &IncompleteBlock{completeBlockSize}
	}
	block := encryptedDataBlock(buffer)
	return &block, nil
}

type commonDataBlock []byte

func (b *commonDataBlock) Version() uint8 {
	return []byte(*b)[versionPosition]
}

func (b *commonDataBlock) HeaderLength() uint8 {
	return []byte(*b)[headerLengthPosition]
}

func (b *commonDataBlock) ContentLength() uint16 {
	return binary.BigEndian.Uint16([]byte(*b)[contentLengthStart:contentLengthEnd])
}

func (b *commonDataBlock) Size() uint {
	completeBlockSize := uint(b.HeaderLength()) + uint(b.ContentLength())
	return completeBlockSize
}

func (b *commonDataBlock) BlockData() []byte {
	return *b
}

type encryptedDataBlock []byte

func (b *encryptedDataBlock) Version() uint8 {
	return []byte(*b)[versionPosition]
}

func (b *encryptedDataBlock) HeaderLength() uint8 {
	return []byte(*b)[headerLengthPosition]
}

func (b *encryptedDataBlock) KeyId() []byte {
	return []byte(*b)[keyIdStart:keyIdEnd]
}

func (b *encryptedDataBlock) Nonce() []byte {
	return []byte(*b)[nonceStart:nonceEnd]
}

func (b *encryptedDataBlock) ContentLength() uint16 {
	return binary.BigEndian.Uint16([]byte(*b)[contentLengthStart:contentLengthEnd])
}

func (b *encryptedDataBlock) DataLength() uint16 {
	return b.ContentLength() - TagSize
}

func (b *encryptedDataBlock) EncryptedData() []byte {
	return []byte(*b)[dataStartEncrypted : dataStartEncrypted+int(b.DataLength())]
}

func (b *encryptedDataBlock) EncryptedDataAndTag() []byte {
	return []byte(*b)[dataStartEncrypted : dataStartEncrypted+int(b.DataLength())+TagSize]
}

func (b *encryptedDataBlock) Tag() []byte {
	tagStart := dataStartEncrypted + int(b.DataLength())
	tagEnd := tagStart + TagSize
	return []byte(*b)[tagStart:tagEnd]
}

func (b *encryptedDataBlock) Size() uint {
	completeBlockSize := HeaderSizeEncrypted + uint(b.ContentLength())
	return completeBlockSize
}

func (b *encryptedDataBlock) BlockData() []byte {
	return *b
}
