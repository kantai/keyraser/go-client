package block

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io"
	"testing"
)

func TestDefaultNonceReaderIsRandom(t *testing.T) {
	assert := assert.New(t)

	nonceReader := GetNonceReader()

	nonce1 := make([]byte, 12)
	io.ReadFull(nonceReader, nonce1)
	nonce2 := make([]byte, 12)
	io.ReadFull(nonceReader, nonce2)

	assert.False(bytes.Equal(nonce1, nonce2))
}

func TestSetNonceReaderReturnsNewNonces(t *testing.T) {
	oldNonceReader := GetNonceReader()
	defer SetNonceReader(oldNonceReader)

	assert := assert.New(t)

	SetNonceReader(bytes.NewReader([]byte{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
	}))

	nonceReader := GetNonceReader()

	nonce1 := make([]byte, 12)
	io.ReadFull(nonceReader, nonce1)
	nonce2 := make([]byte, 12)
	io.ReadFull(nonceReader, nonce2)

	assert.True(bytes.Equal(nonce1, []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}))
	assert.True(bytes.Equal(nonce2, []byte{13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}))
}
