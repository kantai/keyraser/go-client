package stream

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/kantai/keyraser/go-client/pkg/format"
	"io"
)

const headerMarkerString = "KEYRASER"
const markerSize = 8

const formatHeaderLengthSize = 4
const maxFormatHeaderLength = 0xFFFFFF

func WriteHeader(f format.Format, writer io.Writer) error {
	_, err := writer.Write([]byte(headerMarkerString))
	if err != nil {
		return err
	}

	serializedFormatHeader, err := format.SerializeFormat(f)
	if err != nil {
		return err
	}

	formatHeaderLength := len(serializedFormatHeader)
	if formatHeaderLength > maxFormatHeaderLength {
		return fmt.Errorf("format definition exceeds max size %d", maxFormatHeaderLength)
	}

	formatHeaderLengthBuffer := make([]byte, 4)
	binary.BigEndian.PutUint32(formatHeaderLengthBuffer, uint32(formatHeaderLength))
	_, err = writer.Write(formatHeaderLengthBuffer)
	_, err = writer.Write(serializedFormatHeader)

	return err
}

func ReadHeader(reader io.Reader) (format.Format, error) {
	headerSize := markerSize + formatHeaderLengthSize
	header := make([]byte, headerSize)
	n, err := io.ReadFull(reader, header)
	if err != nil {
		return nil, err
	}

	if n < headerSize {
		return nil, fmt.Errorf("can't read header, need %d bytes", headerSize)
	}
	marker := string(header[:markerSize])
	if marker != headerMarkerString {
		return nil, fmt.Errorf("invalid marker, expected %s, found %s", headerMarkerString, marker)
	}

	formatHeaderLength := binary.BigEndian.Uint32(header[markerSize:])
	if formatHeaderLength > maxFormatHeaderLength {
		return nil, fmt.Errorf("format definition exceeds max size %d", maxFormatHeaderLength)
	}

	formatHeaderBuffer := make([]byte, formatHeaderLength)
	n, err = io.ReadFull(reader, formatHeaderBuffer)
	if err != nil {
		return nil, err
	}
	if uint32(n) < formatHeaderLength {
		return nil, fmt.Errorf("can't read format from header, need %d bytes, found %d", formatHeaderLength, n)
	}

	return format.DeserializeFormat(formatHeaderBuffer)
}
