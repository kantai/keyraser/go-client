package stream

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/internal/block"
	"gitlab.com/kantai/keyraser/go-client/pkg/interfaces"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	protocol "gitlab.com/kantai/keyraser/protocol"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"io"
	"sync"
)

const bufferSize = 0xFFFF
const keysPerRequest = 36

func DecryptStream(reader io.Reader, writer item.Writer, connection interfaces.Connection) error {

	var errResult error
	var complete bool

	headerBuffer := []byte{0, 0, 0, 0}
	headerSize := len(headerBuffer)
	for true {

		decKeyRequest := &generated.DecryptionKeyRequest{}
		blocks := make([]block.Block, 0, keysPerRequest)
		for i := 1; i <= 50; i++ {
			n, err := io.ReadFull(reader, headerBuffer)
			if err == io.EOF {
				complete = true
				break
			}
			if n < headerSize {
				return fmt.Errorf("error reading header, missing content")
			}
			blockSize, err := block.GetSizeOfDataBlock(headerBuffer)
			if err != nil {
				return fmt.Errorf("error reading block size: %s", err)
			}
			blockBuffer := make([]byte, blockSize)
			copy(blockBuffer, headerBuffer)
			n, err = io.ReadFull(reader, blockBuffer[headerSize:])
			if uint(headerSize+n) < blockSize {
				return fmt.Errorf("missing data when reading block")
			}
			if err != nil {
				return err
			}
			parsedBlock, err := block.ParseDataBlock(blockBuffer)
			if err != nil {
				return err
			}
			blocks = append(blocks, parsedBlock)

			encBlock, ok := parsedBlock.(block.EncryptedBlock)
			if ok {
				keyId := encBlock.KeyId()
				decKeyRequest.KeyIds = append(decKeyRequest.KeyIds, keyId)
			}
		}

		if len(blocks) > 0 {
			var wg sync.WaitGroup
			wg.Add(1)

			connection.RequestDecryptionKeys(decKeyRequest, func(response *generated.DecryptionKeyResponse) {
				defer wg.Done()
				buffer := make([]byte, bufferSize)
				for _, parsedBlock := range blocks {
					encBlock, ok := parsedBlock.(block.EncryptedBlock)
					if ok {
						keyId := encBlock.KeyId()
						decKey := protocol.GetKeyFromKeyId(response, keyId)
						if decKey == nil {
							log.Debug().Msgf("no key for key id %s", keyId)
							continue
						}

						err := block.DecryptData(encBlock, decKey, buffer)
						if err != nil {
							log.Error().Err(err).Msg("error decrypting data")
							errResult = err
							break
						}

						writer.WriteItem(&item.Item{Data: buffer[:encBlock.DataLength()]})
					}
				}
			}, func(response *generated.ErrorResponse, err error) {
				defer wg.Done()
				if err != nil {
					log.Error().Err(err).Msg("error fetching decryption keys")
					errResult = fmt.Errorf("error fetching decryption keys: %w", err)
					return
				}
			})

			wg.Wait()
		}

		if complete {
			break
		}
	}
	return errResult
}

func EncryptItemsToStream(reader item.Reader, writer io.Writer, connection interfaces.Connection) error {

	var errResult error
	var complete bool
	for true {
		encKeyRequest := &generated.EncryptionKeyRequest{}
		var items []*item.Item
		for i := 1; i <= 50; i++ {
			item, err := reader.ReadItem()
			if err != nil && err != io.EOF {
				return err
			}
			if err == io.EOF {
				complete = true
			}

			if item == nil {
				break
			}

			entityId := item.EntityId
			encKeyRequest.EntityIds = append(encKeyRequest.EntityIds, entityId)

			items = append(items, item)
		}

		if len(items) > 0 {
			var wg sync.WaitGroup
			wg.Add(1)

			connection.RequestEncryptionKeys(encKeyRequest, func(response *generated.EncryptionKeyResponse) {
				defer wg.Done()
				for _, item := range items {
					encKey := protocol.GetKeyFromEntityId(response, []byte(item.EntityId))
					if encKey == nil {
						log.Debug().Msgf("no key for entity %s", item.EntityId)
						continue
					}

					encryptedDataBlock, err := block.EncryptData(item.Data, encKey)
					if err != nil {
						fmt.Print("error encrypting data")
						errResult = err
						break
					}

					writer.Write(encryptedDataBlock.BlockData())
				}
			}, func(response *generated.ErrorResponse, err error) {
				defer wg.Done()
				if err != nil {
					log.Error().Err(err).Msg("error fetching encryption keys")
					errResult = fmt.Errorf("error fetching encryption keys: %w", err)
					return
				}
			})

			wg.Wait()
		}

		if complete {
			break
		}
	}
	return errResult
}
