package stream

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/go-client/internal/connection"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"strings"
	"testing"
)

func TestDecryptEmptyStream(t *testing.T) {
	assert := assert.New(t)

	writer := &item.DummyWriter{}
	reader := bytes.NewReader([]byte{})
	client := &connection.DummyConnection{}

	err := DecryptStream(reader, writer, client)

	assert.Nil(err)
	assert.Len(writer.Items, 0)
}

func TestDecryptStreamWithIncompleteHeader(t *testing.T) {
	assert := assert.New(t)

	writer := &item.DummyWriter{}
	reader := bytes.NewReader([]byte{
		1, 40,
	})
	client := &connection.DummyConnection{}

	err := DecryptStream(reader, writer, client)

	assert.EqualError(err, "error reading header, missing content")
}

func TestDecryptStreamWithIncompleteHeaderData(t *testing.T) {
	assert := assert.New(t)

	writer := &item.DummyWriter{}
	reader := bytes.NewReader([]byte{
		1, 48, 0, 4,
	})
	client := &connection.DummyConnection{}

	err := DecryptStream(reader, writer, client)

	assert.EqualError(err, "missing data when reading block")
}

func TestDecryptStreamWithInvalidTag(t *testing.T) {
	assert := assert.New(t)

	writer := &item.DummyWriter{}
	reader := bytes.NewReader([]byte{
		1, 48, 0, 20,
		// Nonce
		2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
		// KeyId
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		// Data
		4, 4, 4, 4,
		// Tag
		8, 8, 8, 8, 8, 8, 8, 8,
		8, 8, 8, 8, 8, 8, 8, 8,
	})
	client := &connection.DummyConnection{
		DecKey: &generated.DecryptionKey{
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			},
		},
	}

	err := DecryptStream(reader, writer, client)

	assert.EqualError(err, "chacha20poly1305: message authentication failed")
}

func TestDecryptStreamWithMissingKey(t *testing.T) {
	assert := assert.New(t)

	writer := &item.DummyWriter{}
	reader := bytes.NewReader([]byte{
		1, 48, 0, 20,
		// Nonce
		2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
		// KeyId
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		// Data
		4, 4, 4, 4,
		// Tag
		8, 8, 8, 8, 8, 8, 8, 8,
		8, 8, 8, 8, 8, 8, 8, 8,
	})
	client := &connection.DummyConnection{}

	err := DecryptStream(reader, writer, client)

	assert.Nil(err)
	assert.Len(writer.Items, 0)
}

func TestDecryptStreamValidTag(t *testing.T) {
	assert := assert.New(t)

	writer := &item.DummyWriter{}
	reader := bytes.NewReader([]byte{
		1, 48, 0, 24,
		// Nonce
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		// KeyId
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		3, 3, 3, 3, 3, 3, 3, 3,
		// Encrypted data
		0xc9, 0x76, 0x4e, 0x68, 0x5d, 0xda, 0x57, 0x97,

		// Tag,
		0x7d, 0x74, 0x17, 0x26, 0x84, 0xae, 0xfc, 0x74,
		0x2f, 0x30, 0xe7, 0xc2, 0x30, 0xde, 0x6f, 0xb2,
	})
	client := &connection.DummyConnection{
		DecKey: &generated.DecryptionKey{
			KeyBytes: []byte{
				1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
				17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
			},
		},
	}

	err := DecryptStream(reader, writer, client)

	assert.Nil(err)
	assert.Len(writer.Items, 1)
}

func TestEncryptItemsToStreamWithEmptyItems(t *testing.T) {
	assert := assert.New(t)

	reader := &item.DummyReader{}
	writer := &strings.Builder{}
	client := &connection.DummyConnection{}

	err := EncryptItemsToStream(reader, writer, client)

	assert.Nil(err)
	assert.Equal("", writer.String())
}

func TestEncryptItemsToStreamWithSingleItems(t *testing.T) {
	assert := assert.New(t)

	reader := &item.DummyReader{
		Idx: -1,
		Items: []item.Item{
			{EntityId: entity.Id(""), Data: []byte{1, 2, 3, 4}},
		},
	}
	writer := &strings.Builder{}
	client := &connection.DummyConnection{
		EncKey: generated.EncryptionKey{
			KeyId: []byte{1, 2, 3, 4},
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			},
		},
	}

	err := EncryptItemsToStream(reader, writer, client)

	assert.Nil(err)
	assert.Len(writer.String(), 68)
}

func TestEncryptItemsToStreamEncryptionError(t *testing.T) {
	assert := assert.New(t)

	reader := &item.DummyReader{
		Idx: -1,
		Items: []item.Item{
			{EntityId: entity.Id(""), Data: []byte{1, 2, 3, 4}},
		},
	}
	writer := &strings.Builder{}
	client := &connection.DummyConnection{
		EncKey: generated.EncryptionKey{
			KeyId:    []byte{1, 2, 3, 4},
			KeyBytes: []byte{1},
		},
	}

	err := EncryptItemsToStream(reader, writer, client)

	assert.EqualError(err, "invalid key bytes chacha20poly1305: bad key length")
}
