package stream

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/go-client/pkg/format"
	"strings"
	"testing"
)

const testHeaderString = "KEYRASER\x00\x00\x007ndjson:{\"IdProperty\":\"the_id\",\"IdFormat\":\"STRING-UTF8\"}"

func TestWriteHeader(t *testing.T) {
	assert := assert.New(t)

	writer := &strings.Builder{}
	format := format.NDJsonFormat()
	format.IdProperty = "the_id"

	err := WriteHeader(format, writer)

	assert.Nil(err)
	assert.Equal(testHeaderString, writer.String())
}

func TestReadHeader(t *testing.T) {
	assert := assert.New(t)

	reader := strings.NewReader(testHeaderString)
	f, err := ReadHeader(reader)

	assert.Nil(err)
	assert.Equal("ndjson", f.TypeId())

	ndjsonFormat, ok := f.(*format.NDJson)
	assert.True(ok)
	assert.Equal("the_id", ndjsonFormat.IdProperty)
}
