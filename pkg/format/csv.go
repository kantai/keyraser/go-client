package format

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
	"strconv"
	"strings"
)

func CSVFormat() *CSV {
	return &CSV{
		IdField:        "0",
		IdFormat:       entity.StringUtf8Ids,
		Separator:      ";",
		Enclosing:      "",
		ContainsHeader: false,
		Header:         "",
	}
}

type CSV struct {
	IdField        string
	IdFormat       entity.IdFormat
	Separator      string
	Enclosing      string
	ContainsHeader bool
	Header         string
}

func (f *CSV) TypeId() string {
	return "csv"
}

func (f *CSV) CliFlags(flags *flag.FlagSet) {
	flags.StringVar(&f.IdField, "id", f.IdField, "The field name or index to use as entity id")
	entity.DefineIdFormatFlag(flags, &f.IdFormat)
	flags.StringVar(&f.Separator, "separator", f.Separator, "The string that separates the individual fields")
	flags.StringVar(&f.Enclosing, "enclosing", f.Enclosing, "The string that encloses a field value")
	flags.BoolVar(&f.ContainsHeader, "header", f.ContainsHeader, "If the file contains a header as first line")
}

func (f *CSV) Read(reader io.Reader) (item.Reader, error) {
	bufferedReader := bufio.NewReader(reader)
	idFieldIndex := -1

	if f.ContainsHeader {
		lineBytes, err := bufferedReader.ReadBytes('\n')
		if len(lineBytes) > 0 {
			line := string(lineBytes)
			f.Header = line
			fields := strings.Split(line, f.Separator)

			for index, fieldName := range fields {
				if fieldName == f.IdField {
					idFieldIndex = index
					break
				}
			}

			if idFieldIndex == -1 {
				idFieldIndex, err = strconv.Atoi(f.IdField)
				if err != nil || idFieldIndex >= len(fields) {
					return nil, fmt.Errorf("no field with name %s found", f.IdField)
				}
			}
		}
	} else {
		var err error
		idFieldIndex, err = strconv.Atoi(f.IdField)
		if err != nil {
			return nil, err
		}
	}

	return &csvReader{
		fileReader:   bufferedReader,
		separator:    f.Separator,
		idFieldIndex: idFieldIndex,
		idFormat:     f.IdFormat,
		enclosing:    f.Enclosing,
	}, nil
}

func (f *CSV) Write(writer io.Writer) (item.Writer, error) {
	if f.ContainsHeader {
		_, err := writer.Write([]byte(f.Header))
		if err != nil {
			return nil, err
		}
	}

	return &csvWriter{
		fileWriter: writer,
	}, nil
}

type csvReader struct {
	fileReader   *bufio.Reader
	separator    string
	idFieldIndex int
	idFormat     entity.IdFormat
	enclosing    string
}

func (r *csvReader) ReadItem() (*item.Item, error) {
	lineBytes, err := r.fileReader.ReadBytes('\n')
	if len(lineBytes) > 0 {
		line := string(lineBytes)
		fields := strings.Split(line, r.separator)

		if r.enclosing != "" {
			enclosingLen := len(r.enclosing)

			for i, fieldValue := range fields {
				if fieldValue[:enclosingLen] != r.enclosing || fieldValue[len(fieldValue)-enclosingLen:] != r.enclosing {
					return nil, fmt.Errorf("value '%s' not enclosed with '%s'", fieldValue, r.enclosing)
				}
				fields[i] = fieldValue[enclosingLen : len(fieldValue)-enclosingLen]
			}
		}

		if len(fields) <= r.idFieldIndex {
			return nil, fmt.Errorf("no field with index %d", r.idFieldIndex)
		}

		idValue := fields[r.idFieldIndex]
		id, errId := r.idFormat.ParseId(idValue)
		if errId != nil {
			return nil, errId
		}
		return &item.Item{
			EntityId: id,
			Data:     lineBytes,
		}, err
	}
	return nil, err
}

type csvWriter struct {
	fileWriter io.Writer
}

func (w *csvWriter) WriteItem(item *item.Item) error {
	_, err := w.fileWriter.Write(item.Data)
	return err
}
