package format

import (
	"bufio"
	"flag"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
)

func NDJsonFormat() *NDJson {
	return &NDJson{
		IdProperty: "id",
		IdFormat:   entity.StringUtf8Ids,
	}
}

type NDJson struct {
	IdProperty string
	IdFormat   entity.IdFormat
}

func (f *NDJson) TypeId() string {
	return "ndjson"
}

func (f *NDJson) CliFlags(flags *flag.FlagSet) {
	flags.StringVar(&f.IdProperty, "id", "id", "The property name to use as entity id")
	entity.DefineIdFormatFlag(flags, &f.IdFormat)
}

func (f *NDJson) Read(reader io.Reader) (item.Reader, error) {
	bufferedReader := bufio.NewReader(reader)
	return &ndJsonReader{
		fileReader:  bufferedReader,
		idAttribute: f.IdProperty,
		idFormat:    f.IdFormat,
	}, nil
}

func (f *NDJson) Write(writer io.Writer) (item.Writer, error) {
	return &ndJsonWriter{
		fileWriter: writer,
	}, nil
}

type ndJsonReader struct {
	fileReader  *bufio.Reader
	idAttribute string
	idFormat    entity.IdFormat
}

func (r *ndJsonReader) ReadItem() (*item.Item, error) {
	lineBytes, err := r.fileReader.ReadBytes('\n')
	if len(lineBytes) > 0 {
		idValue := jsoniter.Get(lineBytes, r.idAttribute)
		if idValue.LastError() != nil {
			return nil, idValue.LastError()
		}
		id, errId := r.idFormat.ParseId(idValue.ToString())
		if errId != nil {
			return nil, errId
		}
		return &item.Item{
			EntityId: id,
			Data:     lineBytes,
		}, err
	}
	return nil, err
}

type ndJsonWriter struct {
	fileWriter io.Writer
}

func (w *ndJsonWriter) WriteItem(item *item.Item) error {
	_, err := w.fileWriter.Write(item.Data)
	return err
}
