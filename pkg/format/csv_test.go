package format

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
	"strings"
	"testing"
)

func TestCSVFormatDefaults(t *testing.T) {
	assert := assert.New(t)

	format := CSVFormat()

	assert.Equal("0", format.IdField)
	assert.Equal(";", format.Separator)
	assert.Equal("", format.Enclosing)
	assert.False(format.ContainsHeader)
}

func TestCSVFormatReadItemReturnsTheCorrectId(t *testing.T) {
	assert := assert.New(t)

	data := "the id;my value\nid 2;next value\n"
	stringReader := strings.NewReader(data)

	format := CSVFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.Nil(err)
	assert.Equal(entity.Id("the id"), item.EntityId)
	assert.Equal([]byte("the id;my value\n"), item.Data)

	item, err = reader.ReadItem()
	assert.Nil(err)
	assert.Equal(entity.Id("id 2"), item.EntityId)
	assert.Equal([]byte("id 2;next value\n"), item.Data)
}

func TestCSVFormatReaderWorksForLastLineWithoutNewline(t *testing.T) {
	assert := assert.New(t)

	data := "the id;my value"
	stringReader := strings.NewReader(data)

	format := CSVFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()

	assert.Equal(io.EOF, err)
	assert.Equal(entity.Id("the id"), item.EntityId)
	assert.Equal([]byte("the id;my value"), item.Data)
}

func TestCSVFormatReaderSupportsEnclosedFieldValues(t *testing.T) {
	assert := assert.New(t)

	data := "'the id';'my value'"
	stringReader := strings.NewReader(data)

	format := CSVFormat()
	format.Enclosing = "'"

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()

	assert.Equal(io.EOF, err)
	assert.Equal(entity.Id("the id"), item.EntityId)
	assert.Equal([]byte("'the id';'my value'"), item.Data)
}

func TestCSVFormatReadingEmptyFileReturnsEOF(t *testing.T) {
	assert := assert.New(t)

	data := ""
	stringReader := strings.NewReader(data)

	format := CSVFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.Equal(io.EOF, err)
	assert.Nil(item)
}

func TestCSVFormatReaderReturnsErrorWithMissingId(t *testing.T) {
	assert := assert.New(t)

	data := "the id;my value"
	stringReader := strings.NewReader(data)

	format := CSVFormat()
	format.IdField = "2"

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.EqualError(err, "no field with index 2")
	assert.Nil(item)
}

func TestCSVFormatReaderReturnsErrorWithInvalidId(t *testing.T) {
	assert := assert.New(t)

	data := "col1;col2\nthe id;my value"
	stringReader := strings.NewReader(data)

	format := CSVFormat()
	format.IdField = "unknown"
	format.ContainsHeader = true

	reader, err := format.Read(stringReader)

	assert.EqualError(err, "no field with name unknown found")
	assert.Nil(reader)
}

func TestCSVFormatReaderUsesNumericFieldIdWithHeader(t *testing.T) {
	assert := assert.New(t)

	data := "col1;col2\nthe id;my value"
	stringReader := strings.NewReader(data)

	format := CSVFormat()
	format.IdField = "0"
	format.ContainsHeader = true

	reader, err := format.Read(stringReader)

	assert.Nil(err)

	item, err := reader.ReadItem()
	assert.Equal(io.EOF, err)
	assert.Equal(string(item.EntityId), "the id")
}

func TestCSVFormatReaderStoresHeaderInFormat(t *testing.T) {
	assert := assert.New(t)

	data := "col1;col2\nthe id;my value"
	stringReader := strings.NewReader(data)

	format := CSVFormat()
	format.IdField = "0"
	format.ContainsHeader = true

	_, err := format.Read(stringReader)

	assert.Nil(err)
	assert.Equal(format.Header, "col1;col2\n")
}

func TestCSVFormatItemWriter(t *testing.T) {
	assert := assert.New(t)

	format := CSVFormat()
	format.IdField = "0"
	format.ContainsHeader = true
	format.Header = "col1;col2\n"

	item := item.Item{
		EntityId: entity.Id("i1"),
		Data:     []byte("i1:v2"),
	}

	writer := &strings.Builder{}
	itemWriter, err := format.Write(writer)

	err = itemWriter.WriteItem(&item)
	assert.Nil(err)

	assert.Equal("col1;col2\ni1:v2", writer.String())
}
