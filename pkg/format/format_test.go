package format

import (
	"flag"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
	"testing"
)

type parameterlessFormat struct {
}

func (f *parameterlessFormat) TypeId() string {
	return "param-less"
}

func (f *parameterlessFormat) CliFlags(flags *flag.FlagSet) {
}

func (f *parameterlessFormat) Read(reader io.Reader) (item.Reader, error) {
	return nil, nil
}

func (f *parameterlessFormat) Write(writer io.Writer) (item.Writer, error) {
	return nil, nil
}

type formatWithParameter struct {
	MyParameter string
}

func (f *formatWithParameter) TypeId() string {
	return "with-param"
}

func (f *formatWithParameter) CliFlags(flags *flag.FlagSet) {
}

func (f *formatWithParameter) Read(reader io.Reader) (item.Reader, error) {
	return nil, nil
}

func (f *formatWithParameter) Write(writer io.Writer) (item.Writer, error) {
	return nil, nil
}

func TestParameterlessFormatCanBeSerialized(t *testing.T) {
	assert := assert.New(t)

	format := &parameterlessFormat{}

	format_as_json, err := SerializeFormat(format)

	assert.Nil(err)
	assert.Equal(13, len(format_as_json))
	assert.Equal([]byte("param-less:{}"), format_as_json)
}

func TestFormatWithParameterCanBeSerialized(t *testing.T) {
	assert := assert.New(t)

	format := &formatWithParameter{}
	format.MyParameter = "MyValue"

	formatAsJson, err := SerializeFormat(format)

	assert.Nil(err)
	assert.Equal(36, len(formatAsJson))
	assert.Equal([]byte("with-param:{\"MyParameter\":\"MyValue\"}"), formatAsJson)
}

func TestFormatWithParameterCanBeDeserialized(t *testing.T) {
	assert := assert.New(t)

	format := &formatWithParameter{}
	format.MyParameter = ""
	formats[format.TypeId()] = format

	serializedFormat := []byte("with-param:{\"MyParameter\":\"MyValue\"}")

	deserializedFormat, err := DeserializeFormat(serializedFormat)

	assert.Nil(err)

	formatWithParam, formatTypeOk := deserializedFormat.(*formatWithParameter)

	assert.True(formatTypeOk)
	assert.Equal(format.TypeId(), formatWithParam.TypeId())
	assert.Equal("MyValue", formatWithParam.MyParameter)

	delete(formats, "with-param")
}

func TestInvalidFormatHeader(t *testing.T) {
	assert := assert.New(t)

	serializedFormat := []byte("not-a-valid-header")

	deserializedFormat, err := DeserializeFormat(serializedFormat)

	assert.EqualError(err, "invalid format header")
	assert.Nil(deserializedFormat)
}

func TestUnknownFormatDeserialized(t *testing.T) {
	assert := assert.New(t)

	serializedFormat := []byte("unknown-format:{\"MyParameter\":\"MyValue\"}")

	deserializedFormat, err := DeserializeFormat(serializedFormat)

	assert.EqualError(err, "unknown format unknown-format")
	assert.Nil(deserializedFormat)
}

func TestInvalidFormatParameter(t *testing.T) {
	assert := assert.New(t)

	format := &formatWithParameter{}
	format.MyParameter = ""
	formats[format.TypeId()] = format

	serializedFormat := []byte("with-param:not-valid-json")

	deserializedFormat, err := DeserializeFormat(serializedFormat)

	assert.EqualError(err, "error reading format parameters: invalid character 'o' in literal null (expecting 'u')")
	assert.Nil(deserializedFormat)

	delete(formats, "with-param")
}

func TestGetFormats(t *testing.T) {
	assert := assert.New(t)

	formats := GetFormats()
	assert.Equal("block,csv,ndjson,single", formats)
}
