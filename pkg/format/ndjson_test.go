package format

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
	"strings"
	"testing"
)

func TestFormatUsesIdAsDefault(t *testing.T) {
	assert := assert.New(t)

	json := "{\"id\": \"i1\", \"other\": \"value\"}\n{\"id\": \"i2\", \"other\": \"content\"}\n"
	stringReader := strings.NewReader(json)

	format := NDJsonFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.Nil(err)
	assert.Equal(entity.Id("i1"), item.EntityId)
	assert.Equal([]byte("{\"id\": \"i1\", \"other\": \"value\"}\n"), item.Data)

	item, err = reader.ReadItem()
	assert.Nil(err)
	assert.Equal(entity.Id("i2"), item.EntityId)
	assert.Equal([]byte("{\"id\": \"i2\", \"other\": \"content\"}\n"), item.Data)
}

func TestFormatReaderWorksForLastLineWithoutNewline(t *testing.T) {
	assert := assert.New(t)

	json := "{\"id\": \"i1\", \"other\": \"value\"}"
	stringReader := strings.NewReader(json)

	format := NDJsonFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.Equal(io.EOF, err)
	assert.Equal(entity.Id("i1"), item.EntityId)
	assert.Equal([]byte("{\"id\": \"i1\", \"other\": \"value\"}"), item.Data)
}

func TestFormatReadingEmptyFileReturnsEOF(t *testing.T) {
	assert := assert.New(t)

	json := ""
	stringReader := strings.NewReader(json)

	format := NDJsonFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.Equal(io.EOF, err)
	assert.Nil(item)
}

func TestFormatReaderReturnsErrorWithMissingId(t *testing.T) {
	assert := assert.New(t)

	json := "{\"noId\": \"i1\", \"other\": \"value\"}"
	stringReader := strings.NewReader(json)

	format := NDJsonFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.EqualError(err, "[id] not found")
	assert.Nil(item)
}

func TestFormatInvalidJsonReturnsError(t *testing.T) {
	assert := assert.New(t)

	json := "{\"id\": \"i1"
	stringReader := strings.NewReader(json)

	format := NDJsonFormat()

	reader, _ := format.Read(stringReader)

	item, err := reader.ReadItem()
	assert.ErrorContains(err, "readStringSlowPath: unexpected end of input")
	assert.Nil(item)
}

func TestFormatItemWriter(t *testing.T) {
	assert := assert.New(t)

	json := "{\"id\": \"i1\", \"other\": \"value\"}\n"
	item := item.Item{
		EntityId: entity.Id("i1"),
		Data:     []byte(json),
	}
	writer := &strings.Builder{}

	format := NDJsonFormat()

	itemWriter, _ := format.Write(writer)

	err := itemWriter.WriteItem(&item)
	assert.Nil(err)

	assert.Equal("{\"id\": \"i1\", \"other\": \"value\"}\n", writer.String())
}

func TestNdjsonFormatCanBeSerialized(t *testing.T) {
	assert := assert.New(t)

	format := NDJsonFormat()
	format.IdFormat = entity.HexIds
	format.IdProperty = "my-attribute"

	serializedFormat, err := SerializeFormat(format)

	assert.Nil(err)

	deserializedFormat, err := DeserializeFormat(serializedFormat)

	deserializedNdjsonFormat, ok := deserializedFormat.(*NDJson)

	assert.True(ok)
	assert.Equal(deserializedNdjsonFormat.IdFormat.Name, entity.HexIds.Name)
	assert.Equal(deserializedNdjsonFormat.IdProperty, "my-attribute")
}
