package format

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
	"slices"
	"sort"
	"strings"
)

type Format interface {
	TypeId() string
	CliFlags(*flag.FlagSet)
	Read(reader io.Reader) (item.Reader, error)
	Write(writer io.Writer) (item.Writer, error)
}

var formats map[string]Format

func init() {
	formats = make(map[string]Format)
	var f Format = CSVFormat()
	formats[f.TypeId()] = f
	f = NDJsonFormat()
	formats[f.TypeId()] = f
	f = SingleFormat()
	formats[f.TypeId()] = f
	f = BlockFormat()
	formats[f.TypeId()] = f
}

func GetFormats() string {
	keys := make([]string, 0, len(formats))
	for k := range formats {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return strings.Join(keys, ",")
}

func GetFormat(formatType string) (Format, bool) {
	format, exist := formats[formatType]
	return format, exist
}

func SerializeFormat(format Format) ([]byte, error) {
	buffer := []byte(format.TypeId() + ":")
	parameterJson, err := json.Marshal(format)
	if err != nil {
		return nil, err
	}
	return append(buffer, parameterJson...), nil
}

func DeserializeFormat(buffer []byte) (Format, error) {
	pos := slices.Index(buffer, ':')
	if pos < 0 {
		return nil, errors.New("invalid format header")
	}

	formatTypeId := string(buffer[:pos])
	format, exists := GetFormat(formatTypeId)
	if !exists {
		return nil, fmt.Errorf("unknown format %s", formatTypeId)
	}

	formatParameters := buffer[pos+1:]
	err := json.Unmarshal(formatParameters, format)
	if err != nil {
		return nil, fmt.Errorf("error reading format parameters: %w", err)
	}

	return format, nil
}
