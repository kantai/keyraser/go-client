package format

import (
	"errors"
	"flag"
	"gitlab.com/kantai/keyraser/go-client/internal/block"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
)

func SingleFormat() *Single {
	return &Single{
		Id:       "",
		IdFormat: entity.StringUtf8Ids,
	}
}

type Single struct {
	Id       string
	IdFormat entity.IdFormat
}

func (f *Single) TypeId() string {
	return "single"
}

func (f *Single) CliFlags(flags *flag.FlagSet) {
	flags.StringVar(&f.Id, "id", f.Id, "The entity id to use for encrypting the whole file")
	entity.DefineIdFormatFlag(flags, &f.IdFormat)
}

func (f *Single) Read(reader io.Reader) (item.Reader, error) {
	entityId, err := f.IdFormat.ParseId(f.Id)
	if err != nil {
		return nil, err
	}

	return &singleReader{
		reader: reader,
		id:     entityId,
	}, nil
}

func (f *Single) Write(writer io.Writer) (item.Writer, error) {

	return &singleWriter{
		fileWriter: writer,
	}, nil
}

type singleReader struct {
	reader io.Reader
	id     entity.Id
}

func (r *singleReader) ReadItem() (*item.Item, error) {
	buffer := make([]byte, block.MaxEncryptedDataLength)

	n, err := io.ReadFull(r.reader, buffer)
	if errors.Is(err, io.ErrUnexpectedEOF) {
		err = io.EOF
	}
	return &item.Item{
		EntityId: r.id,
		Data:     buffer[:n],
	}, err
}

type singleWriter struct {
	fileWriter io.Writer
}

func (w *singleWriter) WriteItem(item *item.Item) error {
	_, err := w.fileWriter.Write(item.Data)
	return err
}
