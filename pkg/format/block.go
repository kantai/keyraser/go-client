package format

import (
	"errors"
	"flag"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
)

func BlockFormat() *Block {
	return &Block{
		Id:       "",
		IdFormat: entity.StringUtf8Ids,
	}
}

type Block struct {
	Id       string
	IdFormat entity.IdFormat
}

func (f *Block) TypeId() string {
	return "block"
}

func (f *Block) CliFlags(flags *flag.FlagSet) {
	flags.StringVar(&f.Id, "id", f.Id, "The entity id to use for encrypting the block")
	entity.DefineIdFormatFlag(flags, &f.IdFormat)
}

func (f *Block) Read(reader io.Reader) (item.Reader, error) {
	return nil, errors.ErrUnsupported
}

func (f *Block) Write(writer io.Writer) (item.Writer, error) {
	return nil, errors.ErrUnsupported
}

func (f *Block) EntityId() (entity.Id, error) {
	return f.IdFormat.ParseId(f.Id)
}
