package interfaces

import (
	"gitlab.com/kantai/keyraser/auth"
	"gitlab.com/kantai/keyraser/protocol/generated"
)

type ResponseHandler[R interface{}] func(response R)
type ErrorHandler func(response *generated.ErrorResponse, err error)

type Connection interface {
	Credential() auth.ClientCredential
	RequestEncryptionKeys(request *generated.EncryptionKeyRequest, handler ResponseHandler[*generated.EncryptionKeyResponse], errorHandler ErrorHandler) (int64, error)
	RequestDecryptionKeys(request *generated.DecryptionKeyRequest, handler ResponseHandler[*generated.DecryptionKeyResponse], errorHandler ErrorHandler) (int64, error)
	RequestCreateKeys(request *generated.CreateKeyRequest, handler ResponseHandler[*generated.CreateKeyResponse], errorHandler ErrorHandler) (int64, error)
	RequestDeleteKeys(request *generated.DeleteKeyRequest, handler ResponseHandler[*generated.DeleteKeyResponse], errorHandler ErrorHandler) (int64, error)
	RequestKeyPermissions(request *generated.KeyPermissionRequest, handler ResponseHandler[*generated.KeyPermissionResponse], errorHandler ErrorHandler) (int64, error)
	RequestUpdateKeyPermissions(request *generated.UpdateKeyPermissionRequest, handler ResponseHandler[*generated.KeyPermissionResponse], errorHandler ErrorHandler) (int64, error)
	Close()
}
