package entity

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestEmptyIndexDoesntContainAnything(t *testing.T) {
	idsChannel := make(chan Id)

	index := NewIndex(10, idsChannel)

	n := index.Size()

	assert.Equal(t, uint64(0), n)
}

func TestIndexNotCompleteWithoutClosingTheIdSource(t *testing.T) {
	idsChannel := make(chan Id)

	index := NewIndex(10, idsChannel)

	complete := index.IsComplete()

	assert.False(t, complete.Load())

	close(idsChannel)

	time.Sleep(5 * time.Millisecond)
	complete = index.IsComplete()

	assert.True(t, complete.Load())
}

func TestIndexContainsAddedIdsWithHighConfidence(t *testing.T) {
	idsChannel := make(chan Id)

	id1 := Id{1}
	id2 := Id{2}
	id3 := Id{3}
	id4 := Id{4}

	index := NewIndex(10, idsChannel)

	idsChannel <- id1
	idsChannel <- id2
	idsChannel <- id3
	idsChannel <- id4

	close(idsChannel)

	for !index.IsComplete().Load() {
	}

	contained, confidence := index.Contains(id1)
	assert.True(t, contained)
	assert.Greater(t, confidence, float32(0.99))

	contained, confidence = index.Contains(id2)
	assert.True(t, contained)
	assert.Greater(t, confidence, float32(0.99))

	contained, confidence = index.Contains(id3)
	assert.True(t, contained)
	assert.Greater(t, confidence, float32(0.99))

	contained, confidence = index.Contains(id4)
	assert.True(t, contained)
	assert.Greater(t, confidence, float32(0.99))
}

func TestIndexDoesntContainNotAddedIdsWithConfidence1(t *testing.T) {
	idsChannel := make(chan Id)

	id1 := Id{1}
	id2 := Id{2}

	index := NewIndex(10, idsChannel)

	close(idsChannel)

	for !index.IsComplete().Load() {
	}

	contained, confidence := index.Contains(id1)
	assert.False(t, contained)
	assert.Equal(t, confidence, float32(1.0))

	contained, confidence = index.Contains(id2)
	assert.False(t, contained)
	assert.Equal(t, confidence, float32(1.0))
}

func TestIndexSizeIsCorrect(t *testing.T) {
	idsChannel := make(chan Id)

	id1 := Id{1}
	id2 := Id{2}
	id3 := Id{3}
	id4 := Id{4}

	index := NewIndex(10, idsChannel)

	idsChannel <- id1
	idsChannel <- id2
	idsChannel <- id3
	idsChannel <- id4

	close(idsChannel)

	for !index.IsComplete().Load() {
	}

	size := index.Size()

	assert.Equal(t, uint64(4), size)
}

func TestIndexCanBeSavedAndLoaded(t *testing.T) {
	idsChannel := make(chan Id)

	id1 := Id{1}
	id2 := Id{2}
	id3 := Id{3}
	id4 := Id{4}

	index := NewIndex(10, idsChannel)

	idsChannel <- id1
	idsChannel <- id2

	close(idsChannel)

	for !index.IsComplete().Load() {
	}

	buffer := &bytes.Buffer{}
	WriteIndex(index, buffer)

	newIndex, _ := ReadIndex(buffer)

	contained, _ := newIndex.Contains(id1)
	assert.True(t, contained)

	contained, _ = newIndex.Contains(id2)
	assert.True(t, contained)

	contained, _ = newIndex.Contains(id3)
	assert.False(t, contained)

	contained, _ = newIndex.Contains(id4)
	assert.False(t, contained)
}
