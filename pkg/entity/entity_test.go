package entity

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIdInIds(t *testing.T) {
	assert := assert.New(t)

	id := Id{0x01}
	ids := []Id{{0x01}, {0x02}}

	result := id.In(ids)
	assert.True(result)
}

func TestIdNotkInIds(t *testing.T) {
	assert := assert.New(t)

	id := Id{0x01}
	ids := []Id{{0x02}, {0x03}}

	result := id.In(ids)
	assert.False(result)
}

func TestGetIdFormats(t *testing.T) {
	assert := assert.New(t)

	formats := GetIdFormats()

	assert.Len(formats, 2)
	assert.Equal("HEX", formats[0].FormatName())
	assert.Equal("STRING-UTF8", formats[1].FormatName())
}

func TestGetIdFormatNames(t *testing.T) {
	assert := assert.New(t)

	formats := GetIdFormatNames()

	assert.Len(formats, 2)
	assert.Equal("HEX", formats[0])
	assert.Equal("STRING-UTF8", formats[1])
}

func TestGetIdFormatExists(t *testing.T) {
	assert := assert.New(t)

	format, exist := GetIdFormat("HEX")
	assert.True(exist)
	assert.Equal("HEX", format.FormatName())
}

func TestGetIdFormatDoesNotExist(t *testing.T) {
	assert := assert.New(t)

	_, exist := GetIdFormat("UNKNOWN")
	assert.False(exist)
}

func TestHexIdFormatParseIdReturnsErrorIfNotHex(t *testing.T) {
	assert := assert.New(t)

	format := HexIds

	id, err := format.ParseId("1z")
	assert.Nil(id)
	assert.EqualError(err, "encoding/hex: invalid byte: U+007A 'z'")
}

func TestHexIdFormatParseIdReturnsErrorIfOddLength(t *testing.T) {
	assert := assert.New(t)

	format := HexIds

	id, err := format.ParseId("123")
	assert.Nil(id)
	assert.EqualError(err, "encoding/hex: odd length hex string")
}

func TestHexIdFormatParseIdReturnsValid(t *testing.T) {
	assert := assert.New(t)

	format := HexIds

	id, err := format.ParseId("1234")
	assert.Nil(err)
	assert.Equal(Id([]byte{0x12, 0x34}), id)
}

func TestHexIdFormatFormatIdReturnsValidHexString(t *testing.T) {
	assert := assert.New(t)

	format := HexIds

	id := format.FormatId([]byte{0x12, 0x34})
	assert.Equal("1234", id)

	id = format.FormatId([]byte{0xab, 0xcd})
	assert.Equal("abcd", id)
}

func TestHexIdFormatCanBeSerialized(t *testing.T) {
	assert := assert.New(t)

	format := HexIds

	serialized, _ := json.Marshal(format)
	deserialized := IdFormat{}
	err := json.Unmarshal(serialized, &deserialized)

	assert.Nil(err)

	assert.Equal(format.Name, deserialized.Name)

	id := []byte{0x31, 0x32, 0x33, 0x34}
	assert.Equal(format.FormatId(id), deserialized.FormatId(id))

	stringId := "abcd"
	p1, _ := format.ParseId(stringId)
	p2, _ := deserialized.ParseId(stringId)
	assert.Equal(p1, p2)
}

func TestStringIdFormatParseIdReturnsBytesFromString(t *testing.T) {
	assert := assert.New(t)

	format := StringUtf8Ids

	id, err := format.ParseId("1234")
	assert.Nil(err)
	assert.Equal(Id([]byte{0x31, 0x32, 0x33, 0x34}), id)
}

func TestStringIdFormatParseIdReturnsUtf8Encoded(t *testing.T) {
	assert := assert.New(t)

	format := StringUtf8Ids

	id, err := format.ParseId("ä")
	assert.Nil(err)
	assert.Equal(Id([]byte{0xc3, 0xa4}), id)
}

func TestStringIdFormatFormatId(t *testing.T) {
	assert := assert.New(t)

	format := StringUtf8Ids

	id := format.FormatId([]byte{0x31, 0x32, 0x33, 0x34})
	assert.Equal("1234", id)

	id = format.FormatId([]byte{0xc3, 0xa4})
	assert.Equal("ä", id)
}

func TestStringIdFormatCanBeSerialized(t *testing.T) {
	assert := assert.New(t)

	format := StringUtf8Ids

	serialized, _ := json.Marshal(format)
	deserialized := IdFormat{}
	err := json.Unmarshal(serialized, &deserialized)

	assert.Nil(err)

	assert.Equal(format.Name, deserialized.Name)

	id := []byte{0x31, 0x32, 0x33, 0x34}
	assert.Equal(format.FormatId(id), deserialized.FormatId(id))

	stringId := "abcd"
	p1, _ := format.ParseId(stringId)
	p2, _ := deserialized.ParseId(stringId)
	assert.Equal(p1, p2)
}
