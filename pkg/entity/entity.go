package entity

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"strings"
)

type parseFunc func(id string) (Id, error)
type formatFunc func(id Id) string

type IdFormat struct {
	Name     string
	parseId  parseFunc
	formatId formatFunc
}

func (i IdFormat) FormatName() string {
	return i.Name
}

func (i IdFormat) ParseId(id string) (Id, error) {
	return i.parseId(id)
}

func (i IdFormat) FormatId(id Id) string {
	return i.formatId(id)
}

func (i IdFormat) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Name)
}

func (i *IdFormat) UnmarshalJSON(data []byte) (err error) {
	var formatName string
	if err := json.Unmarshal(data, &formatName); err != nil {
		return err
	}
	format, exists := GetIdFormat(formatName)
	if !exists {
		return err
	}
	i.Name = format.Name
	i.parseId = format.parseId
	i.formatId = format.formatId
	return nil
}

const StringUtf8IdFormatName = "STRING-UTF8"

var StringUtf8Ids = IdFormat{
	StringUtf8IdFormatName,
	parseStringUtf8Id,
	formatStringUtf8Id,
}

const HexIdFormatName = "HEX"

var HexIds = IdFormat{
	HexIdFormatName,
	parseHexId,
	formatHexId,
}

type Id []byte
type Ids []Id

func (i Id) In(ids Ids) bool {
	for _, id := range ids {
		if bytes.Equal(id, i) {
			return true
		}
	}
	return false
}

func (i Id) Equals(other Id) bool {
	return bytes.Equal(i, other)
}

var idFormats = []IdFormat{HexIds, StringUtf8Ids}

func GetIdFormats() []IdFormat {
	return idFormats
}

func GetIdFormatNames() []string {
	var formatNames []string
	for _, format := range idFormats {
		formatNames = append(formatNames, format.FormatName())
	}
	return formatNames
}

func GetIdFormat(formatId string) (IdFormat, bool) {
	for _, format := range GetIdFormats() {
		if format.FormatName() == formatId {
			return format, true
		}
	}
	return IdFormat{}, false
}

func DefineIdFormatFlag(flags *flag.FlagSet, idFormat *IdFormat) {
	idFormats := strings.Join(GetIdFormatNames(), ",")
	flags.Func("idFormat", "The format of the entity ids to test ("+idFormats+")", func(s string) error {
		format, exist := GetIdFormat(s)
		if exist {
			*idFormat = format
		} else {
			return fmt.Errorf("unknown format %s", s)
		}
		return nil
	})
}

func parseHexId(id string) (Id, error) {
	idAsBytes, err := hex.DecodeString(id)
	if err != nil {
		return nil, err
	}
	return idAsBytes, nil
}

func formatHexId(id Id) string {
	return hex.EncodeToString(id)
}

func parseStringUtf8Id(id string) (Id, error) {
	return []byte(id), nil
}

func formatStringUtf8Id(id Id) string {
	return string(id)
}
