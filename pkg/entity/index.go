package entity

import (
	"encoding/binary"
	"fmt"
	"github.com/bits-and-blooms/bloom/v3"
	"github.com/rs/zerolog/log"
	"io"
	"sync/atomic"
)

type Index interface {
	add(id Id)
	Contains(id Id) (bool, float32)
	Capacity() uint64
	Size() uint64
	IsComplete() *atomic.Bool
	PrintInfo()
}

func NewIndex(items uint64, idSource <-chan Id) Index {
	idx := newBloomIndex(items)

	go func() {
		for id := range idSource {
			idx.add(id)
		}
		idx.complete.Store(true)
	}()
	return idx
}

func WriteIndex(index Index, writer io.Writer) error {
	switch i := index.(type) {
	case *bloomIndex:
		_, err := writer.Write([]byte(bloomFilterIndexType))
		if err != nil {
			return err
		}
		return saveBloomIndex(i, writer)
	}
	return fmt.Errorf("unknown index type")
}

func ReadIndex(reader io.Reader) (Index, error) {
	headerBuffer := make([]byte, 8)
	_, err := io.ReadFull(reader, headerBuffer)
	if err != nil {
		return nil, err
	}
	indexType := string(headerBuffer)
	switch {
	case indexType == bloomFilterIndexType:
		return loadBloomIndex(reader)
	}
	return nil, fmt.Errorf("unknown index type %s", indexType)
}

const bloomFilterIndexType = "ENTIDXV1"

func newBloomIndex(capacity uint64) *bloomIndex {
	targetFalsePositiveRate := 0.000001
	filter := bloom.NewWithEstimates(uint(capacity), targetFalsePositiveRate)

	return &bloomIndex{
		filter:     filter,
		capacity:   capacity,
		addedItems: 0,
		confidence: float32(1.0 - targetFalsePositiveRate),
	}
}

type bloomIndex struct {
	filter     *bloom.BloomFilter
	capacity   uint64
	addedItems uint64
	confidence float32
	complete   atomic.Bool
}

func (i *bloomIndex) add(id Id) {
	i.filter.Add(id)
	i.addedItems++
}

func (i *bloomIndex) Contains(id Id) (bool, float32) {
	confidence := float32(1.0)
	contained := i.filter.Test(id)
	if contained {
		confidence = i.confidence
	}
	return contained, confidence
}

func (i *bloomIndex) Capacity() uint64 {
	return uint64(i.filter.Cap())
}

func (i *bloomIndex) Size() uint64 {
	return i.addedItems
}

func (i *bloomIndex) IsComplete() *atomic.Bool {
	return &i.complete
}

func saveBloomIndex(index *bloomIndex, writer io.Writer) error {
	err := binary.Write(writer, binary.BigEndian, index.capacity)
	if err != nil {
		return err
	}
	err = binary.Write(writer, binary.BigEndian, uint32(index.filter.Cap()))
	if err != nil {
		return err
	}
	err = binary.Write(writer, binary.BigEndian, uint32(index.filter.K()))
	if err != nil {
		return err
	}
	err = binary.Write(writer, binary.BigEndian, index.addedItems)
	if err != nil {
		return err
	}

	filterSize, err := index.filter.WriteTo(writer)
	log.Info().Msgf("wrote bloom filter index of size %d", filterSize)
	return err
}

func loadBloomIndex(reader io.Reader) (*bloomIndex, error) {
	var capacity uint64
	err := binary.Read(reader, binary.BigEndian, &capacity)
	if err != nil {
		return nil, err
	}
	var bits uint32
	err = binary.Read(reader, binary.BigEndian, &bits)
	if err != nil {
		return nil, err
	}
	var hashFunctions uint32
	err = binary.Read(reader, binary.BigEndian, &hashFunctions)
	if err != nil {
		return nil, err
	}
	var addedItems uint64
	err = binary.Read(reader, binary.BigEndian, &addedItems)
	if err != nil {
		return nil, err
	}
	confidence := float32(1 - bloom.EstimateFalsePositiveRate(uint(bits), uint(hashFunctions), uint(addedItems)))

	filter := bloom.New(uint(bits), uint(hashFunctions))
	_, err = filter.ReadFrom(reader)
	if err != nil {
		return nil, err
	}
	log.Info().Msgf("read bloom filter index with %d items and %f confidence", addedItems, confidence)
	index := &bloomIndex{
		filter:     filter,
		capacity:   capacity,
		addedItems: addedItems,
		confidence: confidence,
		complete:   atomic.Bool{},
	}
	index.complete.Store(true)
	return index, nil
}

func (i *bloomIndex) PrintInfo() {
	fmt.Printf("index type:    bloom filter\n")
	fmt.Printf("est. capacity: %d\n", i.capacity)
	fmt.Printf("size:          %d\n", i.Size())
	fmt.Printf("confidence:    %f\n", i.confidence)
}
