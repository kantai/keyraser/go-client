package commmand

import (
	"flag"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
)

type Command interface {
	CommandId() string
	CliFlags(flags *flag.FlagSet)
	Execute(client client.Client) error
}

var Commands map[string]Command

func init() {
	Commands = make(map[string]Command)
	createCmd := &createCommand{}
	Commands[createCmd.CommandId()] = createCmd
	deleteCmd := &deleteCommand{}
	Commands[deleteCmd.CommandId()] = deleteCmd
	permissionCmd := &permissionCommand{}
	Commands[permissionCmd.CommandId()] = permissionCmd
	setPermissionCmd := &setPermissionCommand{}
	Commands[setPermissionCmd.CommandId()] = setPermissionCmd
}

func CommandIs() []string {
	ids := make([]string, 0, len(Commands))
	for id := range Commands {
		ids = append(ids, id)
	}
	return ids
}

type entityIdsFlags []string

func (i *entityIdsFlags) String() string {
	return "my entity"
}

func (i *entityIdsFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}
