package commmand

import (
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"strings"
)

type createCommand struct {
	entityIds      entityIdsFlags
	entityIdFormat string
}

func (c *createCommand) CommandId() string {
	return "create"
}

func (c *createCommand) CliFlags(flags *flag.FlagSet) {
	flags.Var(&c.entityIds, "id", "A single entity id, can be defined multiple times")
	idFormats := strings.Join(entity.GetIdFormatNames(), ",")
	flags.StringVar(&c.entityIdFormat, "idFormat", "STRING-UTF8", "The format of the id ("+idFormats+")")
}

func (c *createCommand) Execute(client client.Client) error {

	idFormat, exist := entity.GetIdFormat(c.entityIdFormat)
	if !exist {
		return fmt.Errorf("unknown entity id format %s", c.entityIdFormat)
	}

	if len(c.entityIds) > 0 {

		entityIds := make([]entity.Id, 0, len(c.entityIds))
		for _, entityId := range c.entityIds {
			id, err := idFormat.ParseId(entityId)
			if err != nil {
				return err
			}
			entityIds = append(entityIds, id)
		}

		createdEntityIds, err := client.CreateKeys(entityIds)
		if err != nil {
			return err
		}

		for _, entityId := range entityIds {
			if entityId.In(createdEntityIds) {
				log.Info().Msgf("key created for entity %s", idFormat.FormatId(entityId))
			} else {
				log.Info().Msgf("no key created for entity %s", idFormat.FormatId(entityId))
			}
		}
	}
	return nil
}
