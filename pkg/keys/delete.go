package commmand

import (
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"strings"
)

type deleteCommand struct {
	entityIds      entityIdsFlags
	entityIdFormat string
}

func (c *deleteCommand) CommandId() string {
	return "delete"
}

func (c *deleteCommand) CliFlags(flags *flag.FlagSet) {
	flags.Var(&c.entityIds, "id", "A single entity id, can be defined multiple times")
	idFormats := strings.Join(entity.GetIdFormatNames(), ",")
	flags.StringVar(&c.entityIdFormat, "idFormat", "STRING-UTF8", "The format of the id ("+idFormats+")")
}

func (c *deleteCommand) Execute(client client.Client) error {

	idFormat, exist := entity.GetIdFormat(c.entityIdFormat)
	if !exist {
		return fmt.Errorf("unknown entity id format %s", c.entityIdFormat)
	}

	if len(c.entityIds) > 0 {

		entityIds := make([]entity.Id, len(c.entityIds))
		for _, entityId := range c.entityIds {
			id, err := idFormat.ParseId(entityId)
			if err != nil {
				return err
			}
			entityIds = append(entityIds, id)
		}

		deletedEntityIds, err := client.DeleteKeys(entityIds)
		if err != nil {
			return err
		}

		for _, entityId := range entityIds {
			if entityId.In(deletedEntityIds) {
				log.Info().Msgf("key deleted for entity %s", idFormat.FormatId(entityId))
			} else {
				log.Info().Msgf("no key deleted for entity %s", idFormat.FormatId(entityId))
			}
		}
	}
	return nil
}
