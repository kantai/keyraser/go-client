package commmand

import (
	"encoding/binary"
	"encoding/hex"
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/auth"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"strings"
)

type permissionCommand struct {
	entityIds      entityIdsFlags
	entityIdFormat string
}

func (c *permissionCommand) CommandId() string {
	return "permissions"
}

func (c *permissionCommand) CliFlags(flags *flag.FlagSet) {
	flags.Var(&c.entityIds, "id", "A list of entity ids")
	idFormats := strings.Join(entity.GetIdFormatNames(), ",")
	flags.StringVar(&c.entityIdFormat, "id-format", "STRING-UTF8", "The format of the id ("+idFormats+")")
}

func (c *permissionCommand) Execute(cl client.Client) error {

	idFormat, exist := entity.GetIdFormat(c.entityIdFormat)
	if !exist {
		return fmt.Errorf("unknown entity id format %s", c.entityIdFormat)
	}

	if len(c.entityIds) > 0 {

		entityIds := make([]entity.Id, 0, len(c.entityIds))
		for _, entityId := range c.entityIds {
			id, err := idFormat.ParseId(entityId)
			if err != nil {
				return err
			}
			log.Debug().Msgf("adding entity id %s to key permission request", entityId)
			entityIds = append(entityIds, id)
		}

		permissions, err := cl.GetKeyPermissions(entityIds)
		if err != nil {
			return err
		}

		for _, entityId := range entityIds {
			permission, exists := client.PermissionForEntity(permissions, entityId)
			idAsString := idFormat.FormatId(entityId)
			fmt.Printf("Entity %s: ", idAsString)
			if exists {
				fmt.Printf("encrypt: %x  decrypt: %x\n", permission.EncryptPermission, permission.DecryptPermission)
			} else {
				fmt.Print("no key found\n")
			}
		}
	}
	return nil
}

type setPermissionCommand struct {
	entityIds         entityIdsFlags
	entityIdFormat    string
	encryptPermission string
	decryptPermission string
}

func (c *setPermissionCommand) CommandId() string {
	return "set-permissions"
}

func (c *setPermissionCommand) CliFlags(flags *flag.FlagSet) {
	flags.Var(&c.entityIds, "id", "A list of entity ids")
	idFormats := strings.Join(entity.GetIdFormatNames(), ",")
	flags.StringVar(&c.entityIdFormat, "id-format", "STRING-UTF8", "The format of the id ("+idFormats+")")
	flags.StringVar(&c.encryptPermission, "enc-perm", "ffffffffffffffff", "The new encryption permission encoded as hexstring")
	flags.StringVar(&c.decryptPermission, "dec-perm", "ffffffffffffffff", "The new decryption permission encoded as hexstring")
}

func (c *setPermissionCommand) Execute(cl client.Client) error {

	idFormat, exist := entity.GetIdFormat(c.entityIdFormat)
	if !exist {
		return fmt.Errorf("unknown entity id format %s", c.entityIdFormat)
	}

	encPermission, err := hex.DecodeString(c.encryptPermission)
	if err != nil {
		return err
	}
	encPermBytes := make([]byte, 8)
	copy(encPermBytes, encPermission)
	encPermissionInt := binary.LittleEndian.Uint64(encPermBytes)
	log.Debug().Msgf("new encryption permission %s", hex.EncodeToString(encPermBytes))

	decPermission, err := hex.DecodeString(c.decryptPermission)
	if err != nil {
		return err
	}
	decPermBytes := make([]byte, 8)
	copy(decPermBytes, decPermission)
	decPermissionInt := binary.LittleEndian.Uint64(decPermBytes)
	log.Debug().Msgf("new decryption permission %s", hex.EncodeToString(decPermBytes))

	if len(c.entityIds) > 0 {

		newPermissions := make([]client.EntityPermissions, 0, len(c.entityIds))
		entityIds := make([]entity.Id, 0, len(c.entityIds))
		for _, entityId := range c.entityIds {
			id, err := idFormat.ParseId(entityId)
			if err != nil {
				return err
			}
			log.Debug().Msgf("adding entity id %s to key permission request", entityId)
			newPermissions = append(newPermissions, client.EntityPermissions{
				EntityId:          id,
				EncryptPermission: auth.KeyPermission(encPermissionInt),
				DecryptPermission: auth.KeyPermission(decPermissionInt),
			})
			entityIds = append(entityIds, id)
		}

		permissions, err := cl.SetKeyPermissions(newPermissions)
		if err != nil {
			return err
		}

		for _, entityId := range entityIds {
			permission, exists := client.PermissionForEntity(permissions, entityId)
			idAsString := idFormat.FormatId(entityId)
			fmt.Printf("Entity %s: ", idAsString)
			if exists {
				fmt.Printf("encrypt: %x  decrypt: %x\n", permission.EncryptPermission, permission.DecryptPermission)
			} else {
				fmt.Print("no key found\n")
			}
		}
	}
	return nil
}
