package client

import (
	"flag"
	"gitlab.com/kantai/keyraser/go-client/internal/connection"
	"gitlab.com/kantai/keyraser/go-client/pkg/interfaces"
)

type KeyStoreConfig interface {
	DefineFlags(flags *flag.FlagSet)
	Connect() (interfaces.Connection, error)
}

func RemoteConfig() KeyStoreConfig {
	return &connection.RemoteKeyStoreConfig{}
}
