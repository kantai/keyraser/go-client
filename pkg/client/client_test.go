package client

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/auth"
	"gitlab.com/kantai/keyraser/go-client/internal/block"
	connection2 "gitlab.com/kantai/keyraser/go-client/internal/connection"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/format"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"testing"
)

func TestClientEncryptString(t *testing.T) {
	oldNonceReader := block.GetNonceReader()
	defer block.SetNonceReader(oldNonceReader)

	block.SetNonceReader(bytes.NewReader([]byte{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
	}))

	assert := assert.New(t)

	connection := &connection2.DummyConnection{
		EncKey: generated.EncryptionKey{
			KeyId:    []byte{1, 2, 3, 4},
			EntityId: []byte("1234"),
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			},
		},
	}
	client := NewClient(connection)

	reader := bytes.NewReader([]byte("{\"id\":1}"))
	writer := &bytes.Buffer{}
	jsonFormat := format.NDJsonFormat()
	err := client.EncryptStream(jsonFormat, reader, writer)

	assert.Nil(err)
	assert.Equal("KEYRASER\x00\x00\x003ndjson:{\"IdProperty\":\"id\",\"IdFormat\":\"STRING-UTF8\"}\x010\x00\x18\x01\x02\x03\x04\x05\x06\a\b\t\n\v\f\x01\x02\x03\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00ɗ\xf4h\x9c\xbf\xe5\b\x86\x9c\xfeBG\x0eOۍ\xd5@\t\x92\xcej\x17", writer.String())
}

func TestClientDecryptString(t *testing.T) {
	assert := assert.New(t)

	connection := &connection2.DummyConnection{
		DecKey: &generated.DecryptionKey{
			KeyId: []byte{1, 2, 3, 4},
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			},
		},
	}
	client := NewClient(connection)

	reader := bytes.NewReader([]byte("KEYRASER\x00\x00\x003ndjson:{\"IdProperty\":\"id\",\"IdFormat\":\"STRING-UTF8\"}\x010\x00\x18\x01\x02\x03\x04\x05\x06\a\b\t\n\v\f\x01\x02\x03\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00ɗ\xf4h\x9c\xbf\xe5\b\x86\x9c\xfeBG\x0eOۍ\xd5@\t\x92\xcej\x17"))
	writer := &bytes.Buffer{}
	err := client.DecryptStream(reader, writer)

	assert.Nil(err)
	assert.Equal("{\"id\":1}", writer.String())
}

func TestClientEncryptBlock(t *testing.T) {
	oldNonceReader := block.GetNonceReader()
	defer block.SetNonceReader(oldNonceReader)

	block.SetNonceReader(bytes.NewReader([]byte{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
	}))

	assert := assert.New(t)

	connection := &connection2.DummyConnection{
		EncKey: generated.EncryptionKey{
			KeyId:    []byte{1, 2, 3, 4},
			EntityId: []byte("1234"),
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			},
		},
	}
	client := NewClient(connection)

	id := entity.Id("1234")
	buffer := []byte{
		0, 1, 2, 3, 4, 5, 6, 7,
		8, 9, 10, 11, 12, 13, 14, 15,
	}
	blockData, err := client.EncryptBlock(id, buffer)

	assert.Nil(err)
	assert.Len(blockData, 80)
	assert.Equal("\x010\x00 \x01\x02\x03\x04\x05\x06\a\b\t\n\v\f\x01\x02\x03\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xb2\xb4\x9f\x0f\xba\x80\xd2rѹ\x9b\xde(\x1c\xb9V\xcc?;J\r\\\x8aAp\x0eh\x14\xf6;\xa8\xa1", string(blockData))
}

func TestClientEncryptBlockTooLarge(t *testing.T) {
	assert := assert.New(t)

	connection := &connection2.DummyConnection{}
	client := NewClient(connection)

	id := entity.Id("1234")
	buffer := make([]byte, 0xffff)
	_, err := client.EncryptBlock(id, buffer)

	assert.EqualError(err, "too large data length, len (65535) > max (65519)")
}

func TestClientDecryptBlock(t *testing.T) {
	assert := assert.New(t)

	connection := &connection2.DummyConnection{
		DecKey: &generated.DecryptionKey{
			KeyId: []byte{1, 2, 3, 4},
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			},
		},
	}
	client := NewClient(connection)

	blockData := []byte("\x010\x00 \x01\x02\x03\x04\x05\x06\a\b\t\n\v\f\x01\x02\x03\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xb2\xb4\x9f\x0f\xba\x80\xd2rѹ\x9b\xde(\x1c\xb9V\xcc?;J\r\\\x8aAp\x0eh\x14\xf6;\xa8\xa1")
	decData, err := client.DecryptBlock(blockData)

	data := []byte{
		0, 1, 2, 3, 4, 5, 6, 7,
		8, 9, 10, 11, 12, 13, 14, 15,
	}

	assert.Nil(err)
	assert.Len(data, 16)
	assert.Equal(decData, data)
}

func TestClientDecryptBlockInvalidBlocks(t *testing.T) {
	assert := assert.New(t)

	connection := &connection2.DummyConnection{}
	client := NewClient(connection)

	blockData := []byte("")
	_, err := client.DecryptBlock(blockData)

	assert.EqualError(err, "buffer too small to check block size")

	blockData = []byte("\x010\x00 ")
	_, err = client.DecryptBlock(blockData)

	assert.EqualError(err, "incomplete block, required buffer size 80")
}

func TestClientDecryptBlockWithWrongTag(t *testing.T) {
	oldNonceReader := block.GetNonceReader()
	defer block.SetNonceReader(oldNonceReader)

	block.SetNonceReader(bytes.NewReader([]byte{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
	}))

	assert := assert.New(t)

	connection := &connection2.DummyConnection{
		DecKey: &generated.DecryptionKey{
			KeyId: []byte{1, 2, 3, 4},
			KeyBytes: []byte{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2,
			},
		},
	}
	client := NewClient(connection)

	blockData := []byte("\x010\x00 \x01\x02\x03\x04\x05\x06\a\b\t\n\v\f\x01\x02\x03\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xb2\xb4\x9f\x0f\xba\x80\xd2rѹ\x9b\xde(\x1c\xb9V\xcc?;J\r\\\x8aAp\x0eh\x14\xf6;\xa8\xa1")
	_, err := client.DecryptBlock(blockData)

	assert.EqualError(err, "chacha20poly1305: message authentication failed")
}

func TestClientDeleteKeysWithoutPermission(t *testing.T) {
	assert := assert.New(t)

	credential, err := auth.NewCredential("_testdata/client_no_delete.test_kred")
	assert.Nil(err)

	connection := &connection2.DummyConnection{
		Cred: credential.(auth.ClientCredential),
	}
	client := NewClient(connection)

	_, err = client.DeleteKeys([]entity.Id{})

	assert.NotNil(err)
	assert.EqualError(err, "client has no permission for delete-key")
}

func TestClientGetKeyPermissionsWithoutPermission(t *testing.T) {
	assert := assert.New(t)

	credential, err := auth.NewCredential("_testdata/client_no_manage.test_kred")
	assert.Nil(err)

	connection := &connection2.DummyConnection{
		Cred: credential.(auth.ClientCredential),
	}
	client := NewClient(connection)

	_, err = client.GetKeyPermissions([]entity.Id{})

	assert.NotNil(err)
	assert.EqualError(err, "client has no permission for get-permissions")
}

func TestClientSetKeyPermissionsWithoutPermission(t *testing.T) {
	assert := assert.New(t)

	credential, err := auth.NewCredential("_testdata/client_no_manage.test_kred")
	assert.Nil(err)

	connection := &connection2.DummyConnection{
		Cred: credential.(auth.ClientCredential),
	}
	client := NewClient(connection)

	_, err = client.SetKeyPermissions([]EntityPermissions{})

	assert.NotNil(err)
	assert.EqualError(err, "client has no permission for set-permissions")
}
