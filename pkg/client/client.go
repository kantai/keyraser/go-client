package client

import (
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/auth"
	"gitlab.com/kantai/keyraser/go-client/internal/block"
	"gitlab.com/kantai/keyraser/go-client/internal/stream"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"gitlab.com/kantai/keyraser/go-client/pkg/format"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"gitlab.com/kantai/keyraser/protocol"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"io"
	"sync"
)

type Client interface {
	DecryptBlock(block []byte) ([]byte, error)
	EncryptBlock(id entity.Id, buffer []byte) ([]byte, error)
	DecryptStream(reader io.Reader, writer io.Writer) error
	EncryptStream(format format.Format, reader io.Reader, writer io.Writer, extensions ...item.Extension) error
	CreateKeys(entity.Ids) (entity.Ids, error)
	DeleteKeys(entity.Ids) (entity.Ids, error)
	GetKeyPermissions(entity.Ids) ([]EntityPermissions, error)
	SetKeyPermissions([]EntityPermissions) ([]EntityPermissions, error)
}

func NewNoPermission(action string) error {
	return &NoPermission{action: action}
}

type NoPermission struct {
	action string
}

func (e *NoPermission) Error() string {
	return fmt.Sprintf("client has no permission for %s", e.action)
}

type EntityPermissions struct {
	EntityId          entity.Id
	EncryptPermission auth.KeyPermission
	DecryptPermission auth.KeyPermission
}

func PermissionForEntity(permissions []EntityPermissions, id entity.Id) (*EntityPermissions, bool) {
	for _, permission := range permissions {
		if permission.EntityId.Equals(id) {
			return &permission, true
		}
	}
	return nil, false
}

func NewClient(connection Connection) Client {
	return &clientImpl{
		connection: connection,
	}
}

type clientImpl struct {
	connection Connection
}

func (c *clientImpl) DecryptBlock(buffer []byte) ([]byte, error) {

	dataBlock, err := block.ParseDataBlock(buffer)
	if err != nil {
		return nil, err
	}

	encBlock, ok := dataBlock.(block.EncryptedBlock)
	if !ok {
		return nil, errors.New("not an encrypted block")
	}
	keyId := encBlock.KeyId()
	decKeyRequest := &generated.DecryptionKeyRequest{KeyIds: [][]byte{keyId}}

	var wg sync.WaitGroup
	wg.Add(1)

	var errResult error
	result := make([]byte, encBlock.DataLength())
	c.connection.RequestDecryptionKeys(decKeyRequest, func(response *generated.DecryptionKeyResponse) {
		defer wg.Done()
		decKey := protocol.GetKeyFromKeyId(response, keyId)
		if decKey == nil {
			log.Debug().Msgf("no key for key id %s", keyId)

		}

		err := block.DecryptData(encBlock, decKey, result)
		if err != nil {
			log.Debug().Err(err).Msg("error decrypting block")
			errResult = err
		}

	}, func(response *generated.ErrorResponse, err error) {
		defer wg.Done()
		if err != nil {
			log.Error().Err(err).Msg("error fetching decryption keys")
			errResult = fmt.Errorf("error fetching decryption keys: %w", err)
			return
		}
	})

	wg.Wait()
	return result, errResult
}

func (c *clientImpl) EncryptBlock(id entity.Id, buffer []byte) ([]byte, error) {
	encKeyRequest := &generated.EncryptionKeyRequest{EntityIds: [][]byte{id}}

	var wg sync.WaitGroup
	wg.Add(1)

	var errResult error
	var result []byte
	c.connection.RequestEncryptionKeys(encKeyRequest, func(response *generated.EncryptionKeyResponse) {
		defer wg.Done()
		encKey := protocol.GetKeyFromEntityId(response, id)
		if encKey == nil {
			log.Debug().Msgf("no key for entity %s", id)

		}

		encryptedDataBlock, err := block.EncryptData(buffer, encKey)
		if err != nil {
			log.Debug().Err(err).Msg("error encrypting data")
			errResult = err
		} else {
			result = encryptedDataBlock.BlockData()
		}

	}, func(response *generated.ErrorResponse, err error) {
		defer wg.Done()
		if err != nil {
			log.Error().Err(err).Msg("error fetching encryption keys")
			errResult = fmt.Errorf("error fetching encryption keys: %w", err)
			return
		}
	})

	wg.Wait()
	return result, errResult
}

func (c *clientImpl) DecryptStream(reader io.Reader, writer io.Writer) error {
	format, err := stream.ReadHeader(reader)
	if err != nil {
		return err
	}

	itemWriter, err := format.Write(writer)
	return stream.DecryptStream(reader, itemWriter, c.connection)
}

func (c *clientImpl) EncryptStream(format format.Format, reader io.Reader, writer io.Writer, extensions ...item.Extension) error {
	itemReader, err := format.Read(reader)
	if err != nil {
		return err
	}
	for _, extension := range extensions {
		if extension.IsEnabled() {
			itemReader = extension.Extend(itemReader)
		}
	}
	err = stream.WriteHeader(format, writer)
	if err == nil {
		err = stream.EncryptItemsToStream(itemReader, writer, c.connection)
	}
	return err
}

func (c *clientImpl) CreateKeys(entityIds entity.Ids) (entity.Ids, error) {

	if !c.connection.Credential().ClientPermissions().CanCreate() {
		return nil, NewNoPermission("create-key")
	}

	if len(entityIds) == 0 {
		return nil, fmt.Errorf("no entity ids given")
	}
	createKeyRequest := &generated.CreateKeyRequest{}
	for _, entityId := range entityIds {
		createKeyRequest.EntityIds = append(createKeyRequest.EntityIds, entityId)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	var createdEntities entity.Ids
	var errResult error = nil
	c.connection.RequestCreateKeys(createKeyRequest, func(response *generated.CreateKeyResponse) {
		defer wg.Done()
		for _, entityId := range entityIds {
			created := false
			for _, key := range response.Keys {
				if bytes.Equal(key.EntityId, entityId) {
					created = true
				}
			}
			if created {
				log.Debug().Msgf("key created for entity %s", hex.EncodeToString(entityId))
				createdEntities = append(createdEntities, entityId)
			} else {
				log.Debug().Msgf("no key created for entity %s", hex.EncodeToString(entityId))
			}
		}
	}, func(response *generated.ErrorResponse, err error) {
		defer wg.Done()
		if err != nil {
			log.Error().Err(err).Msg("error creating keys")
			errResult = fmt.Errorf("error deleting keys: %w", err)
			return
		}
	})

	wg.Wait()
	return createdEntities, errResult
}

func (c *clientImpl) DeleteKeys(entityIds entity.Ids) (entity.Ids, error) {

	if !c.connection.Credential().ClientPermissions().CanDelete() {
		return nil, NewNoPermission("delete-key")
	}

	if len(entityIds) == 0 {
		return nil, fmt.Errorf("no entity ids given")
	}
	deleteKeyRequest := &generated.DeleteKeyRequest{}
	for _, entityId := range entityIds {
		deleteKeyRequest.EntityIds = append(deleteKeyRequest.EntityIds, entityId)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	var deletedEntities entity.Ids
	var errResult error = nil
	c.connection.RequestDeleteKeys(deleteKeyRequest, func(response *generated.DeleteKeyResponse) {
		defer wg.Done()
		for _, entityId := range entityIds {
			deleted := false
			for _, key := range response.Keys {
				if bytes.Equal(key.EntityId, entityId) {
					deleted = true
				}
			}
			if deleted {
				log.Debug().Msgf("key deleted for entity %s", hex.EncodeToString(entityId))
				deletedEntities = append(deletedEntities, entityId)
			} else {
				log.Debug().Msgf("no key deleted for entity %s", hex.EncodeToString(entityId))
			}
		}
	}, func(response *generated.ErrorResponse, err error) {
		defer wg.Done()
		if err != nil {
			log.Error().Err(err).Msg("error deleting keys")
			errResult = fmt.Errorf("error deleting keys: %w", err)
			return
		}
	})

	wg.Wait()
	return deletedEntities, errResult
}

func (c *clientImpl) GetKeyPermissions(entityIds entity.Ids) ([]EntityPermissions, error) {

	if !c.connection.Credential().ClientPermissions().CanManage() {
		return nil, NewNoPermission("get-permissions")
	}

	var errResult error = nil
	if len(entityIds) == 0 {
		return nil, fmt.Errorf("no entity ids given")
	}
	keyPermissionRequest := &generated.KeyPermissionRequest{}
	for _, entityId := range entityIds {
		entityIds = append(entityIds, entityId)
		keyPermissionRequest.EntityIds = append(keyPermissionRequest.EntityIds, entityId)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	var entityPermissions []EntityPermissions

	c.connection.RequestKeyPermissions(keyPermissionRequest, func(response *generated.KeyPermissionResponse) {
		defer wg.Done()

		entityPermissions = make([]EntityPermissions, 0, len(response.Permissions))
		for _, permission := range response.Permissions {
			entityPermission := EntityPermissions{
				EntityId:          permission.EntityId,
				EncryptPermission: auth.KeyPermission(permission.EncryptPermission),
				DecryptPermission: auth.KeyPermission(permission.DecryptPermission),
			}
			entityPermissions = append(entityPermissions, entityPermission)
		}
	}, func(response *generated.ErrorResponse, err error) {
		defer wg.Done()
		if err != nil {
			log.Error().Err(err).Msg("error getting key permissions")
			errResult = fmt.Errorf("error getting key permissions: %w", err)
			return
		}
	})

	wg.Wait()
	return entityPermissions, errResult
}

func (c *clientImpl) SetKeyPermissions(entityPermissions []EntityPermissions) ([]EntityPermissions, error) {

	if !c.connection.Credential().ClientPermissions().CanManage() {
		return nil, NewNoPermission("set-permissions")
	}

	var errResult error = nil
	if len(entityPermissions) == 0 {
		return nil, fmt.Errorf("no entity ids given")
	}
	updateKeyPermissionRequest := &generated.UpdateKeyPermissionRequest{}
	for _, entityPermission := range entityPermissions {
		updateKeyPermissionRequest.Permissions = append(updateKeyPermissionRequest.Permissions,
			&generated.KeyPermission{
				EntityId:          entityPermission.EntityId,
				EncryptPermission: int64(entityPermission.EncryptPermission),
				DecryptPermission: int64(entityPermission.DecryptPermission),
			},
		)
	}

	var wg sync.WaitGroup
	wg.Add(1)

	var returnedEntityPermissions []EntityPermissions

	c.connection.RequestUpdateKeyPermissions(updateKeyPermissionRequest, func(response *generated.KeyPermissionResponse) {
		defer wg.Done()

		returnedEntityPermissions = make([]EntityPermissions, 0, len(response.Permissions))
		for _, permission := range response.Permissions {
			entityPermission := EntityPermissions{
				EntityId:          permission.EntityId,
				EncryptPermission: auth.KeyPermission(permission.EncryptPermission),
				DecryptPermission: auth.KeyPermission(permission.DecryptPermission),
			}
			returnedEntityPermissions = append(returnedEntityPermissions, entityPermission)
		}
	}, func(response *generated.ErrorResponse, err error) {
		defer wg.Done()
		if err != nil {
			log.Error().Err(err).Msg("error setting key permissions")
			errResult = fmt.Errorf("error setting key permissions: %w", err)
			return
		}
	})

	wg.Wait()
	return returnedEntityPermissions, errResult
}
