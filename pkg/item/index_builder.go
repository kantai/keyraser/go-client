package item

import (
	"flag"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"io"
	"os"
)

func NewIndexBuilder() *IndexBuilder {

	return &IndexBuilder{}
}

type IndexBuilder struct {
	src           Reader
	Index         entity.Index
	idChannel     chan entity.Id
	indexFilePath string
	indexFile     *os.File
	itemsEstimate uint64
}

func (b *IndexBuilder) CLIFlags(flags *flag.FlagSet) {
	flags.StringVar(&b.indexFilePath, "index", "", "The path to the file where the index is stored.")
	flags.Uint64Var(&b.itemsEstimate, "indexItems", 0, "The estimated number of items.")
}

func (b *IndexBuilder) IsEnabled() bool {
	return b.indexFilePath != ""
}

func (b *IndexBuilder) Extend(reader Reader) Reader {

	b.idChannel = make(chan entity.Id)
	b.Index = entity.NewIndex(b.itemsEstimate, b.idChannel)
	b.src = reader
	indexFile, err := os.OpenFile(b.indexFilePath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}
	b.indexFile = indexFile
	return b
}

func (b *IndexBuilder) ReadItem() (*Item, error) {
	item, err := b.src.ReadItem()
	if err != nil {
		defer b.indexFile.Close()
		close(b.idChannel)
		if err == io.EOF {
			for {
				if b.Index.IsComplete().Load() {
					break
				}
			}
			err := entity.WriteIndex(b.Index, b.indexFile)
			if err != nil {
				panic(err)
			}
		}
		return nil, err
	}
	b.idChannel <- item.EntityId
	return item, nil
}
