package item

import (
	"flag"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"io"
)

type Item struct {
	EntityId entity.Id
	Data     []byte
}

type Reader interface {
	ReadItem() (*Item, error)
}

type Writer interface {
	WriteItem(*Item) error
}

type Extension interface {
	CLIFlags(flags *flag.FlagSet)
	IsEnabled() bool
	Extend(reader Reader) Reader
}

type DummyReader struct {
	Items []Item
	Idx   int
}

func (r *DummyReader) ReadItem() (*Item, error) {
	r.Idx++
	if r.Idx >= len(r.Items) {
		return nil, io.EOF
	}
	return &r.Items[r.Idx], nil
}

type DummyWriter struct {
	Items []*Item
}

func (r *DummyWriter) WriteItem(item *Item) error {
	r.Items = append(r.Items, item)
	return nil
}
