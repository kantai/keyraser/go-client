package item

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"io"
	"testing"
)

func TestIndexBuilderAddsIdsToIndex(t *testing.T) {

	reader := Reader(&DummyReader{
		Items: []Item{
			{EntityId: entity.Id{1}},
			{EntityId: entity.Id{2}},
			{EntityId: entity.Id{3}},
			{EntityId: entity.Id{4}},
		},
		Idx: -1,
	})

	builder := NewIndexBuilder()
	builder.itemsEstimate = 10
	builder.indexFilePath = t.TempDir() + "/test.index"

	reader = builder.Extend(reader)

	for {
		_, err := reader.ReadItem()
		if err == io.EOF {
			break
		}
	}

	exist, _ := builder.Index.Contains(entity.Id{1})
	assert.True(t, exist)

	exist, _ = builder.Index.Contains(entity.Id{2})
	assert.True(t, exist)

	exist, _ = builder.Index.Contains(entity.Id{3})
	assert.True(t, exist)

	exist, _ = builder.Index.Contains(entity.Id{4})
	assert.True(t, exist)

	exist, _ = builder.Index.Contains(entity.Id{5})
	assert.False(t, exist)
}
