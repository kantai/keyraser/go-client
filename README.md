# Keyraser Go client

This repository contains client tools that are used for using keyraser.

## Getting started

### Checkout the repository

```
$ git clone https://gitlab.com/kantai/keyraser/go-client.git
```

### Build the client tools

```
$ go build -o kr-encrypt cmd/encrypt/main.go
$ go build -o kr-decrypt cmd/decrypt/main.go
$ go build -o kr-index cmd/index/main.go
$ go build -o kr-keys cmd/keys/main.go
```

## How to use the tools

### Encrypting data

```
cat my-data.json | ./kr-encrypt ndjson -id user_id -credential client.kred > my-data.enc
```

`encrypt` reads the input data from `stdin` and writes the encrypted data out
to `stdout`. For this, `kr-encrypt` needs to understand the format of the data
in order to be able to extract the id to use for requesting the encryption
key.

The first argument of `kr-encrypt` is the name of the format of the data that
should be encrypted. Currently `kr-encrypt` supports only new-line delimited
json, where individual items are formatted as one json object per line.

`-id` is a format specific argument which tells encrypt, from which
JSON property the id for determining the encryption key is being read.
This depends on the actual input data.

`-credential` is a required argument that points to a credential file for
authenticating the client against a keystore.

### Decrypting data

```
cat my-data.enc | ./kr-decrypt -credential client.kred > my-data.dec
```

`kr-decrypt` reads the encrypted input data from `stdin` and writes the decrypted
data out to `stdout`.

`-credential` is a required argument that points to a credential file for
authenticating the client against a keystore.
