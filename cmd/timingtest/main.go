package main

import (
	"fmt"
	"github.com/rs/zerolog/log"
	block2 "gitlab.com/kantai/keyraser/go-client/internal/block"
	"gitlab.com/kantai/keyraser/go-client/internal/utils"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
	protocol "gitlab.com/kantai/keyraser/protocol"
	"gitlab.com/kantai/keyraser/protocol/generated"
	"os"
	"time"

	"sync"
)

const numberOfKeys = 50

func main() {
	config := client.RemoteConfig()
	utils.SetupLogging()

	client, err := config.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect")
	}

	encKeyRequest := &generated.EncryptionKeyRequest{
		EntityIds: [][]byte{
			{1},
			{2},
			{3},
			{4},
			{5},
			{6},
			{7},
			{8},
			{9},
			{10},
			{11},
			{12},
			{13},
			{14},
			{15},
			{16},
			{17},
			{18},
			{19},
			{20},
			{21},
			{22},
			{23},
			{24},
			{25},
			{26},
			{27},
			{28},
			{29},
			{30},
			{31},
			{32},
			{33},
			{34},
			{35},
			{36},
			{37},
			{38},
			{39},
			{40},
			{41},
			{42},
			{43},
			{44},
			{45},
			{46},
			{47},
			{48},
			{49},
			{50},
		},
	}

	var wg sync.WaitGroup
	wg.Add(1)

	var dataElements [][]byte
	for i := byte(1); i <= 50; i++ {
		dataElements = append(dataElements, []byte{
			i, i, i, i, i, i, i, i,
		})
	}

	var encryptedData [][]byte
	var keyIds [][]byte

	client.RequestEncryptionKeys(encKeyRequest, func(response *generated.EncryptionKeyResponse) {
		defer wg.Done()
		for _, data := range dataElements {
			encKey := protocol.GetKeyFromEntityId(response, data[:1])
			if encKey == nil {
				log.Info().Msgf("no key for entity %s", data)
				continue
			}

			dataBlock, err := block2.EncryptData(data, encKey)
			if err != nil {
				log.Error().Err(err).Msg("error encrypting data")
				continue
			}

			encryptedData = append(encryptedData, dataBlock.BlockData())
			keyIds = append(keyIds, encKey.KeyId)
		}
	}, nil)

	wg.Wait()

	decKeyRequest := &generated.DecryptionKeyRequest{
		KeyIds: [][]byte{},
	}

	for _, keyId := range keyIds {
		decKeyRequest.KeyIds = append(decKeyRequest.KeyIds, keyId)
	}

	startTime := time.Now()
	numberOfRequests := 10000
	for requestNumber := 1; requestNumber < numberOfRequests; requestNumber++ {

		wg.Add(1)

		client.RequestDecryptionKeys(decKeyRequest, func(response *generated.DecryptionKeyResponse) {
			defer wg.Done()
			buffer := make([]byte, 8)
			for i, encData := range encryptedData {
				parsedBlock, err := block2.ParseDataBlock(encData)
				encBlock := parsedBlock.(block2.EncryptedBlock)
				if err != nil {
					log.Error().Err(err).Msg("error parsing data block")
				}
				decKey := protocol.GetKeyFromKeyId(response, encBlock.KeyId())
				if decKey == nil {
					log.Error().Msgf("no key for data block %d", i)
					continue
				}

				err = block2.DecryptData(encBlock, decKey, buffer)
				if err != nil {
					log.Error().Err(err).Msg("error decrypting data")
					continue
				}

			}
			if requestNumber%100 == 0 {
				fmt.Fprintf(os.Stderr, "response for request %d", requestNumber)
			}
		}, nil)

		wg.Wait()
	}
	endTime := time.Now()
	duration := endTime.Sub(startTime)
	fmt.Fprintf(os.Stderr, "finished after %d requests in %f s", numberOfRequests, duration.Seconds())
}
