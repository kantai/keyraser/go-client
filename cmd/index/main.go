package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/internal/utils"
	"gitlab.com/kantai/keyraser/go-client/pkg/entity"
	"os"
)

var indexPath string
var idFormat = entity.StringUtf8Ids

func parseCli() string {
	if len(os.Args) == 1 {
		fmt.Fprintf(os.Stderr, "Usage: %s <command>\n", os.Args[0])
		fmt.Fprint(os.Stderr, "supported commands: info,test\n")
		os.Exit(0)
	}
	command := os.Args[1]
	flags := flag.NewFlagSet("", flag.ExitOnError)
	flags.StringVar(&indexPath, "index", "", "The path to the file containing the index")
	utils.DefineLoggingFlags(flags)
	switch command {
	case "info":
	case "test":
		entity.DefineIdFormatFlag(flags, &idFormat)
	default:
		log.Fatal().Msgf("invalid command %s", command)
		os.Exit(1)
	}
	if err := flags.Parse(os.Args[2:]); err != nil {
		log.Fatal().Err(err).Msg("error parsing parameters")
		os.Exit(2)
	}
	flags.Parse(os.Args[2:])
	return command
}

func main() {
	cmd := parseCli()
	utils.SetupLogging()

	indexFile, err := os.Open(indexPath)
	if err != nil {
		log.Fatal().Err(err).Msgf("no index file %s", indexPath)
		os.Exit(2)
	}
	defer indexFile.Close()

	index, err := entity.ReadIndex(indexFile)
	if err != nil {
		log.Fatal().Err(err).Msgf("error reading index %s", indexPath)
		os.Exit(2)
	}

	switch cmd {
	case "info":
		index.PrintInfo()
	case "test":
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			id := scanner.Text()
			entityId, err := idFormat.ParseId(id)
			if err != nil {
				fmt.Fprintf(os.Stderr, "invalid entity id %v", err)
				os.Exit(3)
			}
			contained, confidence := index.Contains(entityId)
			fmt.Printf("%t: conf %f\n", contained, confidence)
		}
	}
}
