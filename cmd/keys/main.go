package main

import (
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/internal/utils"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
	command "gitlab.com/kantai/keyraser/go-client/pkg/keys"
	"os"
	"strings"
)

func parseCli(config client.KeyStoreConfig) command.Command {
	if len(os.Args) == 1 {
		commandIds := strings.Join(command.CommandIs(), ",")
		fmt.Fprintf(os.Stderr, "Usage: %s <command>\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "supported commands: %s\n", commandIds)
		os.Exit(0)
	}
	commandId := os.Args[1]
	cmd, exist := command.Commands[commandId]
	if !exist {
		log.Fatal().Msgf("invalid command %s", commandId)
		os.Exit(1)
	}
	flags := flag.NewFlagSet("", flag.ExitOnError)
	utils.DefineLoggingFlags(flags)
	cmd.CliFlags(flags)
	config.DefineFlags(flags)
	if err := flags.Parse(os.Args[2:]); err != nil {
		log.Fatal().Err(err).Msg("error parsing parameters")
		os.Exit(2)
	}
	return cmd
}

func main() {
	config := client.RemoteConfig()
	cmd := parseCli(config)
	utils.SetupLogging()

	connection, err := config.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect")
	}
	defer connection.Close()
	//startTime := time.Now()

	client := client.NewClient(connection)

	err = cmd.Execute(client)
	if err != nil {
		log.Fatal().Err(err).Msg("error executing command")
	}

	//endTime := time.Now()
	//duration := endTime.Sub(startTime)
	//log.Printf("finished after %f s", duration.Seconds())
}
