package main

import (
	"flag"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/internal/utils"
	client "gitlab.com/kantai/keyraser/go-client/pkg/client"
	"io"
	"os"
)

var formatType string

func parseCli(config client.KeyStoreConfig) bool {
	var blockDecrypt bool
	flags := flag.NewFlagSet("", flag.ExitOnError)
	utils.DefineLoggingFlags(flags)
	utils.DefineIoFlags(flags)
	config.DefineFlags(flags)

	firstArg := 1
	if os.Args[1] == "block" {
		blockDecrypt = true
		firstArg = 2
	}
	if err := flags.Parse(os.Args[firstArg:]); err != nil {
		log.Fatal().Err(err).Msg("error parsing parameters")
		os.Exit(2)
	}
	return blockDecrypt
}

func main() {
	config := client.RemoteConfig()
	blockDecrypt := parseCli(config)
	utils.SetupLogging()

	connection, err := config.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect")
	}
	defer connection.Close()

	//startTime := time.Now()

	reader, err := utils.InputFile()
	if err != nil {
		log.Fatal().Err(err).Msgf("error opening input file")
	}
	writer, err := utils.OutputFile()
	if err != nil {
		log.Fatal().Err(err).Msgf("error opening output file")
	}
	client := client.NewClient(connection)

	if !blockDecrypt {
		err = client.DecryptStream(reader, writer)
		if err != nil {
			log.Fatal().Err(err).Msgf("error decrypting stream")
		}
	} else {
		buffer, err := io.ReadAll(reader)
		if err != nil {
			log.Fatal().Err(err).Msg("error reading encrypted data")
		}
		decBlock, err := client.DecryptBlock(buffer)
		if err != nil {
			log.Fatal().Err(err).Msg("error decrypting block")
		}
		_, err = writer.Write(decBlock)
		if err != nil {
			log.Fatal().Err(err).Msg("error writing decrypted data")
		}
	}

	//endTime := time.Now()
	//duration := endTime.Sub(startTime)
	//log.Printf("finished after %f s", duration.Seconds())
}
