package main

import (
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kantai/keyraser/go-client/internal/utils"
	"gitlab.com/kantai/keyraser/go-client/pkg/client"
	"gitlab.com/kantai/keyraser/go-client/pkg/format"
	"gitlab.com/kantai/keyraser/go-client/pkg/item"
	"io"
	"os"
)

var formatType string

func parseCli(config client.KeyStoreConfig, extensions ...item.Extension) format.Format {
	if len(os.Args) == 1 {
		fmt.Fprintf(os.Stderr, "Usage: %s <format>\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "supported formats: %s\n", format.GetFormats())
		os.Exit(0)
	}
	formatType = os.Args[1]
	format, exist := format.GetFormat(formatType)
	if !exist {
		log.Fatal().Msgf("invalid format %s", formatType)
		os.Exit(1)
	}
	flags := flag.NewFlagSet("", flag.ExitOnError)
	utils.DefineLoggingFlags(flags)
	utils.DefineIoFlags(flags)
	format.CliFlags(flags)
	config.DefineFlags(flags)
	for _, extension := range extensions {
		extension.CLIFlags(flags)
	}
	if err := flags.Parse(os.Args[2:]); err != nil {
		log.Fatal().Err(err).Msg("error parsing parameters")
		os.Exit(2)
	}
	return format
}

func main() {
	config := client.RemoteConfig()
	indexBuilder := item.NewIndexBuilder()
	frmt := parseCli(config, indexBuilder)
	utils.SetupLogging()

	connection, err := config.Connect()
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect")
	}
	defer connection.Close()

	client := client.NewClient(connection)

	//startTime := time.Now()

	reader, err := utils.InputFile()
	if err != nil {
		log.Fatal().Err(err).Msgf("error opening input file")
	}
	writer, err := utils.OutputFile()
	if err != nil {
		log.Fatal().Err(err).Msgf("error opening output file")
	}

	blockFormat, isBlockFormat := frmt.(*format.Block)
	if !isBlockFormat {
		err = client.EncryptStream(frmt, reader, writer, indexBuilder)
		if err != nil {
			log.Fatal().Err(err).Msg("error encrypting")
		}
	} else {
		buffer, err := io.ReadAll(reader)
		if err != nil {
			log.Fatal().Err(err).Msg("error reading data")
		}
		entityId, err := blockFormat.EntityId()
		if err != nil {
			log.Fatal().Err(err).Msg("error parsing entity id")
		}
		encBuffer, err := client.EncryptBlock(entityId, buffer)
		if err != nil {
			log.Fatal().Err(err).Msg("error encrypting block")
		}
		_, err = writer.Write(encBuffer)
		if err != nil {
			log.Fatal().Err(err).Msg("error writing encrypted block data")
		}
	}

	//endTime := time.Now()
	//duration := endTime.Sub(startTime)
	//log.Printf("finished after %f s", duration.Seconds())
}
